from django.shortcuts import render
from rest_framework import viewsets
# from .permissions import ReadOnly
from .models import (
    Certificates,
    Education,
    Experience,
    Speciality
)

from .serializers import (
    CertificatesSerializer,
    EducationSerializer,
    ExperienceSerializer,
    SpecialitySerializer
)
import django_filters
from rest_framework import pagination

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.request import Request

class AllPagination(pagination.PageNumberPagination):

    page_size_query_param = 'page_size'





class CertificatesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    queryset = Certificates.objects.all()
    serializer_class = CertificatesSerializer

class EducationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    queryset = Education.objects.all()
    serializer_class = EducationSerializer

class ExperienceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Experience.objects.all()
    serializer_class = ExperienceSerializer


class SpecialityViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    queryset = Speciality.objects.all()
    serializer_class = SpecialitySerializer
    pagination_class = AllPagination
