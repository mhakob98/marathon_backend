
from django.urls import path,include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'certificates', views.CertificatesViewSet)
router.register(r'experience', views.ExperienceViewSet)
router.register(r'education', views.EducationViewSet)
router.register(r'speciality', views.SpecialityViewSet)


urlpatterns = [
    path('', include(router.urls)),

]

