from django.db import models

# Create your models here.

class Speciality(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=True,blank=True, max_length=1000)

    def __str__(self):
        return self.name
    

class Certificates(models.Model):
    id = models.AutoField(primary_key=True)
    description = models.TextField(null=True,blank=True)
    file = models.CharField(null=True,blank=True, max_length=1000)

    def __str__(self):
        return str(self.id)


class Education(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=True,blank=True, max_length=1000)
    specialization = models.CharField(null=True,blank=True, max_length=1000)
    start_date = models.CharField(null=True,blank=True, max_length=1000)
    end_date = models.CharField(null=True,blank=True, max_length=1000)

    

    class Meta:
        verbose_name = "Education"
        verbose_name_plural = "Educations"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Education_detail", kwargs={"pk": self.pk})


class Experience(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(null=True,blank=True, max_length=1000)
    specialization = models.CharField(null=True,blank=True, max_length=1000)
    start_date = models.CharField(null=True,blank=True, max_length=1000)
    end_date = models.CharField(null=True,blank=True, max_length=1000)

    class Meta:
        verbose_name = "Experience"
        verbose_name_plural = "Experiences"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Experience_detail", kwargs={"pk": self.pk})
