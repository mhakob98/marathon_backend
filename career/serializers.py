from rest_framework import serializers
from .models import (
    Certificates,
    Education,
    Experience,
    Speciality
)

class CertificatesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Certificates  
        fields = '__all__'


class EducationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Education  
        fields = '__all__'

class ExperienceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Experience  
        fields = '__all__'

class SpecialitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Speciality  
        fields = '__all__'

