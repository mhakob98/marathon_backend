from rest_framework import serializers
from client.serializers import UserSerializer,ClientSerializer
from coach.serializers import CoachSerializer,CoachSerializerLocal
from .models import (
    Feed,
    Feedback,
    Tag,
    Like,
    FeedMedia,
    FeedCategory,
    BookMarked,
    Comment,
    LikeComment,
    DisLikeComment,
    Follower,
    TagCategory
)
from django.db.models import Q

class FeedbackSerializer(serializers.HyperlinkedModelSerializer):
    user_information = UserSerializer(source="user",read_only=True)
    class Meta:
        model = Feedback  
        fields = (
            'id',
            'comment',
            'rate',
            'user',
            'coach',
            'user_information'
        )

class TagCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TagCategory  
        fields = '__all__'


class TagSerializer(serializers.HyperlinkedModelSerializer):
    category_rel = TagCategorySerializer(source="category",read_only=True)
    class Meta:
        model = Tag  
        fields = ('url','id','name','category','category_rel')


class FeedCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FeedCategory  
        fields = '__all__'

class FeedMediaSerializerLoc(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FeedMedia  
        fields = '__all__'


class LikeSerializerLoc(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Like  
        fields = '__all__'

class FeedSerializer(serializers.HyperlinkedModelSerializer):
    tag_information = TagSerializer(source='tag',many=True,read_only=True)
    creator_info = CoachSerializer(source='creator',read_only=True)
    creator_client_info = ClientSerializer(source='creator_client',read_only=True)
    feed_media = FeedMediaSerializerLoc(many=True,read_only=True)
    feed_likes_count = serializers.SerializerMethodField()
    feed_comments_count = serializers.SerializerMethodField()
    is_liked = serializers.SerializerMethodField()
    is_liked_id = serializers.SerializerMethodField()

    class Meta:
        model = Feed  
        fields = (
            'id' ,
            'url',
            'creator' ,
            'creator_info',
            'timeStamp' ,
            'duration' ,
            'category',
            'title',
            'tag' ,
            'tag_information',
            'creator_client',
            'creator_client_info',
            'feed_media',
            'feed_likes_count',
            'feed_comments_count',
            'is_liked',
            'is_public',
            'is_liked_id'
        )

    def get_feed_likes_count(self, obj):
        return obj.feed_like.all().count()

    def get_feed_comments_count(self, obj):
        return obj.feed_comment.all().count()

    def get_is_liked(self, obj):
        request = self.context['request']
        user = request.user
        if obj.feed_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None))).count() > 0:
            return True
        else :
            return False

    def get_is_liked_id(self, obj):
        request = self.context['request']
        user = request.user
        res = obj.feed_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None)))
        if res.count() > 0:
            return res[0].id
        else :
            return False



class FeedMediaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FeedMedia  
        fields = '__all__'


class LikeSerializer(serializers.HyperlinkedModelSerializer):
    liked_user = CoachSerializerLocal(source='coach',read_only=True)
    liked_coach = CoachSerializerLocal(source='user',read_only=True)

    class Meta:
        model = Like  
        fields = ('url','id','feed','user','liked_user','coach','liked_coach')


class CommentSerializerLoc(serializers.HyperlinkedModelSerializer):
    comment_coach = CoachSerializerLocal(source='coach',read_only=True)
    user_coach = ClientSerializer(source='user',read_only=True)
    likes_count = serializers.SerializerMethodField()
    dislikes_count = serializers.SerializerMethodField()
    is_liked = serializers.SerializerMethodField()
    is_dis_liked = serializers.SerializerMethodField()

    is_liked_id = serializers.SerializerMethodField()
    is_dis_liked_id = serializers.SerializerMethodField()

    class Meta:
        model = Comment  
        fields = (
            'id',
            'url',
            'feed',
            'user',
            'user_coach',
            'coach',
            'comment_coach',
            'parent',
            'is_liked',
            'is_dis_liked',
            'is_liked_id',
            'is_dis_liked_id',
            'message','likes_count','dislikes_count','crated_at')

    def get_likes_count(self, obj):
        return obj.comment_like.all().count()

    def get_dislikes_count(self, obj):
        return obj.comment_dis_like.all().count()

    def get_is_liked(self, obj):
        request = self.context['request']
        user = request.user
        if obj.comment_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None))).count() > 0  :
            return True

        else :
            return False

    def get_is_liked_id(self, obj):
        request = self.context['request']
        user = request.user
        res = obj.comment_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None)))
        if res.count() > 0  :
            return res[0].id

        else :
            return False

    def get_is_dis_liked(self,obj):
        request = self.context['request']
        user = request.user
        if obj.comment_dis_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None))).count() > 0  :
            return True

        else :
            return False

    def get_is_dis_liked_id(self,obj):
        request = self.context['request']
        user = request.user
        res = obj.comment_dis_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None)))
        
        if res.count() > 0  :
            return res[0].id

        else :
            return False

class CommentSerializer(serializers.HyperlinkedModelSerializer):
    comment_coach = CoachSerializerLocal(source='coach',read_only=True)
    user_coach = ClientSerializer(source='user',read_only=True)
    comment_sub = CommentSerializerLoc(read_only=True,many=True)
    likes_count = serializers.SerializerMethodField()
    dislikes_count = serializers.SerializerMethodField()
    is_liked = serializers.SerializerMethodField()
    is_dis_liked = serializers.SerializerMethodField()

    is_liked_id = serializers.SerializerMethodField()
    is_dis_liked_id = serializers.SerializerMethodField()
    class Meta:
        model = Comment  
        fields = (
            'id',
            'url',
            'feed',
            'user',
            'user_coach',
            'coach',
            'comment_coach',
            'parent',
            'is_liked',         
            'is_dis_liked',
            'is_liked_id',
            'is_dis_liked_id','comment_sub','message','likes_count','dislikes_count','crated_at')

    def get_likes_count(self, obj):
        return obj.comment_like.all().count()

    def get_dislikes_count(self, obj):
        return obj.comment_dis_like.all().count()

    def get_is_liked(self, obj):
        request = self.context['request']
        user = request.user
        if obj.comment_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None))).count() > 0  :
            return True

        else :
            return False

    def get_is_liked_id(self, obj):
        request = self.context['request']
        user = request.user
        res = obj.comment_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None)))
        if res.count() > 0  :
            return res[0].id

        else :
            return False

    def get_is_dis_liked(self,obj):
        request = self.context['request']
        user = request.user
        if obj.comment_dis_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None))).count() > 0  :
            return True

        else :
            return False

    def get_is_dis_liked_id(self,obj):
        request = self.context['request']
        user = request.user
        res = obj.comment_dis_like.filter(Q(Q(user__user_id=user.id) & ~Q(user__user_id=None)) |Q(Q(coach__user_id=user.id) & ~Q(coach__user_id=None)))
        
        if res.count() > 0  :
            return res[0].id

        else :
            return False

class BookMarkedSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BookMarked  
        fields = '__all__'



class LikeCommentSerializer(serializers.HyperlinkedModelSerializer):
    comment_liked_user = CoachSerializerLocal(source='coach',read_only=True)
    comment_liked_coach = ClientSerializer(source='user',read_only=True)

    class Meta:
        model = LikeComment  
        fields = ('url','id','comment','user','comment_liked_user','coach','comment_liked_coach')


class DisLikeCommentSerializer(serializers.HyperlinkedModelSerializer):
    comment_dis_liked_user = CoachSerializerLocal(source='coach',read_only=True)
    comment_dis_liked_coach = ClientSerializer(source='user',read_only=True)

    class Meta:
        model = DisLikeComment  
        fields = ('url','id','comment','user','comment_dis_liked_user','coach','comment_dis_liked_coach')

class FollowerSerializer(serializers.HyperlinkedModelSerializer):
    whom_user_follower = ClientSerializer(source='whom_user',read_only=True)
    whom_coach_follower = CoachSerializerLocal(source='whom_coach',read_only=True)
    who_user_follower = ClientSerializer(source='who_user',read_only=True)
    who_coach_follower = CoachSerializerLocal(source='who_coach',read_only=True)

    class Meta:
        model = Follower  
        fields = ('url','id','whom_user','whom_coach','who_user','who_coach','whom_user_follower','whom_coach_follower','who_user_follower','who_coach_follower')

class FollowerSerializerLoc(serializers.HyperlinkedModelSerializer):
    whom_user_follower = ClientSerializer(source='whom_user',read_only=True)
    whom_coach_follower = CoachSerializerLocal(source='whom_coach',read_only=True)


    class Meta:
        model = Follower  
        fields = ('url','id','whom_user','whom_coach','who_user','who_coach','whom_user_follower','whom_coach_follower')