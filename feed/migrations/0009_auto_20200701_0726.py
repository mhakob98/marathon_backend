# Generated by Django 2.1.13 on 2020-07-01 07:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('feed', '0008_feed_creator_client'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feed',
            name='creator',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator_info', to='coach.Coach'),
        ),
        migrations.AlterField(
            model_name='feed',
            name='creator_client',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='creator_client_info', to='client.Client'),
        ),
        migrations.AlterField(
            model_name='feedmedia',
            name='feed',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='feed_media', to='feed.Feed'),
        ),
    ]
