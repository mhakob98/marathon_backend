# Generated by Django 2.0.13 on 2020-03-10 10:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('feed', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='feed',
            name='tag',
            field=models.ManyToManyField(related_name='tag_information', to='feed.Tag'),
        ),
    ]
