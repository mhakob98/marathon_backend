from django.shortcuts import render
from rest_framework import viewsets
from .permissions import ReadOnly
from .models import (
    Feed,
    Feedback,
    Tag,
    FeedMedia,
    Like,
    FeedCategory,
    BookMarked,
    Client,
    Coach,
    Comment,
    LikeComment,
    DisLikeComment,
    Follower,
    TagCategory
)

from .serializers import (
    FeedbackSerializer,
    FeedSerializer,
    TagSerializer,
    FeedMediaSerializer,
    LikeSerializer,
    FeedCategorySerializer,
    BookMarkedSerializer,
    CommentSerializer,
    LikeCommentSerializer,
    DisLikeCommentSerializer,
    FollowerSerializer,
    TagCategorySerializer
)
import django_filters
from rest_framework import pagination
from django.db.models import Q

from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.test import APIRequestFactory
from rest_framework.request import Request
from coach.serializers import (
    CoachDetailSerializer
)
from client.serializers import (
    ClientSerializer
)

factory = APIRequestFactory()

request = factory.get('/')


serializer_context = {

    'request': Request(request)

}
class FeedPagination(pagination.PageNumberPagination):

    page_size_query_param = 'page_size'


# Create your views here.
class FeedFilter(django_filters.FilterSet):
    class Meta:
        model = Feed
        fields = ["creator","category","creator_client","is_public"]

class CommentFilter(django_filters.FilterSet):
    class Meta:
        model = Comment
        fields = ["feed"]


class FollowerFilter(django_filters.FilterSet):
    class Meta:
        model = Follower
        fields = ["whom_user",'whom_coach','who_user','who_coach']

class LikeFilter(django_filters.FilterSet):
    class Meta:
        model = Like
        fields = ["feed"]


class DisLikeCommentFilter(django_filters.FilterSet):
    class Meta:
        model = DisLikeComment
        fields = ["comment"]

class LikeCommentFilter(django_filters.FilterSet):
    class Meta:
        model = LikeComment
        fields = ["comment"]


class FeedbackFilter(django_filters.FilterSet):
    class Meta:
        model = Feedback
        fields = ["coach"]

class TagFilter(django_filters.FilterSet):
    class Meta:
        model = Tag
        fields = ["category","name"]

class TagCategoryFilter(django_filters.FilterSet):
    class Meta:
        model = TagCategory
        fields = ["name","title"]


class FeedbackViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = FeedbackFilter

class FeedViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Feed.objects.all().order_by('-id')
    serializer_class = FeedSerializer
    # filter_backends = [filters.DjangoFilterBackend]
    # filterset_class = FeedFilter
    pagination_class = FeedPagination

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        creator = self.request.query_params.get('creator',None)
        category= self.request.query_params.get('category',None)
        creator_client = self.request.query_params.get('creator_client',None)

        kwargs = {}
        if creator:
            kwargs[ '{0}_{1}'.format('creator', 'id')] = creator

        if category:
            kwargs[ '{0}_{1}'.format('category', 'id')] = category
        if creator_client:
            kwargs[ '{0}_{1}'.format('creator_client', 'id')] = creator_client
        print(kwargs,'----------------',self.action)
        if self.action != 'list':
            return Feed.objects.all().order_by('-id')

        elif self.request.query_params.get('all',None):
            return Feed.objects.filter(Q(is_public=True)&Q(**kwargs)).order_by('-id')

        elif self.request.query_params.get('me',None):
            return Feed.objects.filter(**kwargs).order_by('-id')  
        else:
            return Feed.objects.filter(Q(is_public=True)|Q(**kwargs)).order_by('-id')
class TagViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = TagFilter

class TagCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    queryset = TagCategory.objects.all()
    serializer_class = TagCategorySerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = TagCategoryFilter

class FeedCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    queryset = FeedCategory.objects.all()
    serializer_class = FeedCategorySerializer

class FeedMediaViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = FeedMedia.objects.all()
    serializer_class = FeedMediaSerializer


class LikeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    filterset_class = LikeFilter
    queryset = Like.objects.all()
    serializer_class = LikeSerializer


class LikeCommentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    filterset_class = LikeCommentFilter
    queryset = LikeComment.objects.all()
    serializer_class = LikeCommentSerializer

class DisLikeCommentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    filterset_class = DisLikeCommentFilter
    queryset = DisLikeComment.objects.all()
    serializer_class = DisLikeCommentSerializer

class CommentViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)

    queryset = Comment.objects.filter(parent=None)
    serializer_class = CommentSerializer
    filterset_class = CommentFilter




class FollowerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)

    queryset = Follower.objects.all()
    serializer_class = FollowerSerializer
    filterset_class = FollowerFilter


class BookMarkedViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    queryset = BookMarked.objects.all()
    serializer_class = BookMarkedSerializer


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def create_feed(request):
    title = request.data.get('title')
    content = request.data.get('content')
    is_public = request.data.get('is_public')
    role = request.data.get('role')
    sending_date ={
        "title":title,
        "creator_client" : "",
        "is_public" : is_public
    }
    feed = ''
    print(request.user,'--------------------',is_public)

    if role == 'client':
        queryset = Client.objects.get(user=request.user)
        client_info = ClientSerializer(instance=queryset,  context={   'request' : request})
        sending_date['creator_client'] = client_info.data.get('url')
    else:
        queryset = Coach.objects.get(user=request.user)
        coach_info = CoachDetailSerializer(instance=queryset,  context={   'request' : request})
        sending_date['creator'] = coach_info.data.get('url')

    print(sending_date,'-----------')
    feed = FeedSerializer(
            data=dict(sending_date),
            context=serializer_context
        )

    if feed.is_valid():
        feed.save()
        print(feed.data,'---------------')
        feed_url = feed.data.get('url')
        feed_media= FeedMediaSerializer(data={
            "feed":feed_url,
            "content" :content

        })
        if feed_media.is_valid():
            feed_media.save()
        else:
            return Response(feed_media.errors, status=400)

        return Response(feed.data, status=200)

    else:
        return Response(feed.errors, status=400)
