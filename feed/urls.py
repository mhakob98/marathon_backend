
from django.urls import path,include
from feed import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'feedback', views.FeedbackViewSet)
router.register(r'feeds', views.FeedViewSet)
router.register(r'feed-category', views.FeedCategoryViewSet)
router.register(r'tag', views.TagViewSet)
router.register(r'tag-category', views.TagCategoryViewSet)

router.register(r'like', views.LikeViewSet)
router.register(r'comment', views.CommentViewSet)
router.register(r'dislike-comment', views.DisLikeCommentViewSet)
router.register(r'like-comment', views.LikeCommentViewSet)


router.register(r'book-marked', views.BookMarkedViewSet)
router.register(r'feed-media', views.FeedMediaViewSet)
router.register(r'follower', views.FollowerViewSet)



urlpatterns = [
    path('', include(router.urls)),
    path('create', views.create_feed)


    

]

