from django.db import models
from coach.models import Coach
from client.models import Client
# Create your models here.
class Feedback(models.Model):
    id = models.AutoField(primary_key=True)
    comment = models.CharField(max_length=255)
    rate = models.IntegerField()
    # user => REF
    user = models.ForeignKey(Client,on_delete=models.CASCADE,related_name="user_information",null=True)
    coach = models.ForeignKey(Coach,on_delete=models.CASCADE)

class FeedCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255,null=True)

    def __str__(self):
        return self.name

class TagCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    title = models.TextField(null=True,blank=True)

    def __str__(self):
        return self.name 

class Tag(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    category =  models.ForeignKey(TagCategory,null=True,blank=True, on_delete=models.CASCADE,related_name="category_rel")

    def __str__(self):
        return self.name 


class Feed(models.Model):
    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(Coach,related_name='creator_info',on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,related_name='creator_client_info',on_delete=models.CASCADE,null=True,blank=True)
    timeStamp = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    title = models.CharField(null=True,blank=True,max_length=1000)
    duration = models.IntegerField(null=True,blank=True)
    is_public = models.BooleanField(null=True,blank=True,default=False)
    category = models.ForeignKey(FeedCategory,on_delete=models.CASCADE,null=True,blank=True)
    tag = models.ManyToManyField(Tag,related_name='tag_information',null=True,blank=True)

class FeedMedia(models.Model):
    id = models.AutoField(primary_key=True)
    path = models.CharField(max_length=255,null=True,blank=True)
    thumbnail = models.CharField(max_length=255,null=True,blank=True)
    aspect_ratio = models.FloatField(null=True,blank=True)
    type = models.CharField(max_length=255,null=True,blank=True)
    content = models.TextField(null=True,blank=True)
    feed = models.ForeignKey(Feed,on_delete=models.CASCADE,related_name='feed_media',null=True,blank=True)

class Like(models.Model):
    id = models.AutoField(primary_key=True)
    feed = models.ForeignKey(Feed,on_delete=models.CASCADE,related_name='feed_like')
    user = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True,related_name='liked_user')
    coach = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True,related_name='liked_coach')


class Follower(models.Model):
    id = models.AutoField(primary_key=True)
    whom_user = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True,related_name='whom_user_follower')
    whom_coach = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True,related_name='whom_coach_follower')
    who_user = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True,related_name='who_user_follower')
    who_coach = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True,related_name='who_coach_follower')

class BookMarked(models.Model):
    id = models.AutoField(primary_key=True)
    feed = models.ForeignKey(Feed,on_delete=models.CASCADE)
    user = models.ForeignKey(Client,on_delete=models.CASCADE)
    coach = models.ForeignKey(Coach,on_delete=models.CASCADE)

class Comment(models.Model):

    id = models.AutoField(primary_key=True)
    feed = models.ForeignKey(Feed,on_delete=models.CASCADE,related_name='feed_comment')
    user = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True,related_name='comment_user')
    coach = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True,related_name='comment_coach')
    parent = models.ForeignKey('Comment',on_delete=models.CASCADE,null=True,blank=True,related_name='comment_sub')
    message = models.TextField(null=True,blank=True)
    crated_at = models.DateTimeField(auto_now_add=True,blank=True,null=True)


    class Meta:
        verbose_name = "Comment"
        verbose_name_plural = "Comments"
    def __str__(self):
        return str(self.id)

    def get_absolute_url(self):
        return reverse("Comment_detail", kwargs={"pk": self.pk})


class LikeComment(models.Model):
    id = models.AutoField(primary_key=True)
    comment = models.ForeignKey(Comment,on_delete=models.CASCADE,related_name='comment_like')
    user = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True,related_name='comment_liked_user')
    coach = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True,related_name='comment_liked_coach')

class DisLikeComment(models.Model):
    id = models.AutoField(primary_key=True)
    comment = models.ForeignKey(Comment,on_delete=models.CASCADE,related_name='comment_dis_like')
    user = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True,related_name='comment_dis_liked_user')
    coach = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True,related_name='comment_dis_liked_coach')