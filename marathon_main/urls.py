
from django.urls import path,include
from marathon_main import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register('marathon',views.MarathonViewSet)
router.register('question',views.QuestionViewSet)
router.register('marathon-tag',views.MarathonTagViewSet)
router.register('user-join-marathon',views.UserJoinMarathonViewSet)
router.register('marathon-join-marathonPlan',views.MarathonJoinMarathonPlanViewSet)
router.register('marathon-plan',views.MarathonPlanViewSet)
router.register('marathon-plan-item',views.MarathonPlanItemViewSet)
router.register('measurement',views.MeasurementViewSet)
router.register('user-measurement',views.UserMeasurementViewSet)
router.register('motivations',views.MotivationViewSet)



urlpatterns = [
    path('', include(router.urls))
]
