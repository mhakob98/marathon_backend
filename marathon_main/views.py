from rest_framework import viewsets
from .permissions import ReadOnly
from django_filters import rest_framework as filters
from .models import (
   Marathon,
   MarathonTag,
   Question,
   MarathonPlan,
   MarathonPlanItem,
   Measurement,
   UserMeasurement,
   MarathonMotivations,
   UserJoinMarathon,
   MarathonJoinMarathonPlan
)
import django_filters
import subprocess
from .serializers import (
    MarathonSerializer,
    QuestionSerializer,
    MarathonTagSerializer,
    MarathonPlanSerializer,
    MarathonPlanItemSerializer,
    MeasurementSerializer,
    UserMeasurementSerializer,
    MotivationSerializer,
    MarathonByIdSerializer,
    UserJoinMarathonSerializer,
    MarathonJoinMarathonPlanSerializer
)

import os
from .forms import UploadFileForm
from rest_framework.decorators import api_view
from django.http import JsonResponse
from .models import FileUpliad
# from moviepy.editor import *

class CoachFilter(django_filters.FilterSet):
    class Meta:
        model = Marathon
        fields = ["creator"]

class MarathonViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Marathon.objects.all()
    serializer_class = MarathonSerializer
    detail_serializer_class = MarathonByIdSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = CoachFilter
    def get_serializer_class(self):
        print(self.action)
        if self.action == 'retrieve':
            if hasattr(self, 'detail_serializer_class'):
                return self.detail_serializer_class

        return super(MarathonViewSet, self).get_serializer_class()
        

class MotivationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = MarathonMotivations.objects.all()
    serializer_class = MotivationSerializer

class QuestionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

class MarathonTagViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = MarathonTag.objects.all()
    serializer_class = MarathonTagSerializer


class MarathonPlanViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = MarathonPlan.objects.all()
    serializer_class = MarathonPlanSerializer

class MarathonPlanItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = MarathonPlanItem.objects.all()
    serializer_class = MarathonPlanItemSerializer

class MeasurementViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Measurement.objects.all()
    serializer_class = MeasurementSerializer

class UserMeasurementViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = UserMeasurement.objects.all()
    serializer_class = UserMeasurementSerializer

class MarathonJoinMarathonPlanViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = MarathonJoinMarathonPlan.objects.all()
    serializer_class = MarathonJoinMarathonPlanSerializer


class RouteFilter(django_filters.FilterSet):
    marathon_join_marathon_plan__marathon = django_filters.NumberFilter(lookup_expr='exact')

    class Meta:
        model = UserJoinMarathon
        fields = ["marathon_join_marathon_plan__marathon"]


class UserJoinMarathonViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = UserJoinMarathon.objects.all()
    serializer_class = UserJoinMarathonSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = RouteFilter








@api_view(['POST'])
def upload_file(request):
    request_keys = request.FILES.keys()
    print(request.FILES)
    request_key = list(request_keys)[0]
    form = UploadFileForm(request.POST, request.FILES)
    if form.is_valid():
        if request_key == 'file':
            instance = FileUpliad(file=request.FILES[str(request_key)])
            instance.save()
            return JsonResponse({'message' : "file upload successfully",'file_name': instance.file.name,'file_name_tumbl': 'ssssssss'})
        elif request_key == 'video':
            instance = FileUpliad(video=request.FILES[str(request_key)])
            base_path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..','media/video_tumbl/'))
            instance.save()
            subprocess.call('ffmpeg -i "' + instance.video.path+'" -ss 00:00:40 -vframes 1 -f image2 "'+str(base_path)+'/'+instance.video.name+'.jpg'+'"',shell=True )
            instance.video_thumbnail = instance.video.name+'.jpg'
            instance.save()
            return JsonResponse({'message' : "file upload successfully",'file_name': instance.video.name,'file_name_tumbl': instance.video_thumbnail})
        return JsonResponse({'message' : "file upload successfully",'file_name': None,'file_name_tumbl': None})
    else:
        return JsonResponse({'message': "file upload failed"},status = 400)

