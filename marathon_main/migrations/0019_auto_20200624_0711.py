# Generated by Django 2.1.13 on 2020-06-24 07:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marathon_main', '0018_auto_20200407_1128'),
    ]

    operations = [
        migrations.AddField(
            model_name='fileupliad',
            name='video',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
        migrations.AddField(
            model_name='fileupliad',
            name='video_thumbnail',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
        migrations.AlterField(
            model_name='fileupliad',
            name='file',
            field=models.ImageField(blank=True, null=True, upload_to=''),
        ),
    ]
