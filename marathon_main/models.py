from django.db import models
from coach.models import Coach
from utils.models import Language
from client.models import Client
from utils.models import Unit
from django.utils import timezone
from calendar_marathon.models import MarathonCalendarScheduler
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill

class MarathonTag(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class MarathonMotivations(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField()

    def __str__(self):
        return self.name

class Question(models.Model):
    id = models.AutoField(primary_key=True)
    question = models.TextField()
    type = models.CharField(max_length=255)
    regex = models.CharField(max_length=255)
    is_register = models.BooleanField()

    def __str__(self):
        return self.question

class Measurement(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    unit = models.ForeignKey(Unit,on_delete=models.CASCADE,null=True)
    image = models.CharField(max_length=1000,null=True,blank=True)
    is_goal = models.BooleanField()
    is_validated = models.BooleanField()


class Marathon(models.Model):
    id = models.AutoField(primary_key=True)
    start = models.DateTimeField(default=timezone.now, null=True)
    end = models.DateTimeField(default=timezone.now, null=True)
    info = models.TextField()
    title = models.CharField(max_length=255)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,related_name='coach_marathon')
    banner = models.CharField(max_length=1000,null=True,blank=True)
    # weight_start = models.IntegerField(null=True)
    # weight_actual = models.IntegerField(null=True)
    # weight_target = models.IntegerField(null=True)
    price_sale = models.IntegerField(null=True)
    discriminator= models.IntegerField(null=True,blank=True)
    banners = models.TextField(null=True,blank=True)
    recentScore = models.IntegerField(null=True,blank=True)
    bookmark = models.BooleanField(null=True,blank=True)
    questions =  models.TextField(null=True,blank=True)
    price_amount = models.FloatField(null=True)
    # banner_ratio = models.CharField(max_length=255)
    language = models.ManyToManyField(Language,related_name='marathon_languages')
    calendar_scheduler = models.ManyToManyField(MarathonCalendarScheduler,related_name='marathon_calendar_scheduler')
    marathon_tag = models.TextField(null=True,blank=True)
    marathon_question = models.ManyToManyField(Question)
    # marathon_measurement = models.ManyToManyField(Measurement)
    motivations = models.ManyToManyField(MarathonMotivations,null=True,blank=True)
    
class MarathonPlan(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

class MarathonPlanItem(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class MarathonJoinMarathonPlan(models.Model):
    id = models.AutoField(primary_key=True)
    marathon = models.ForeignKey(Marathon,on_delete=models.CASCADE) 
    Marathon_plan = models.ForeignKey(MarathonPlan,on_delete=models.CASCADE)
    price = models.FloatField()
    participant_count = models.IntegerField()
    marathon_plan_item = models.ManyToManyField(MarathonPlanItem)


class UserJoinMarathon(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(Client,on_delete=models.CASCADE,related_name="user_info")
    marathon_join_marathon_plan = models.ForeignKey(MarathonJoinMarathonPlan,on_delete=models.CASCADE)
    date = models.DateTimeField()


class UserMeasurement(models.Model):
    id = models.AutoField(primary_key=True)
    meausurment = models.ForeignKey(Measurement,on_delete=models.CASCADE)
    timestamp = models.TimeField()
    value = models.FloatField()
    marathon = models.ForeignKey(Marathon,on_delete=models.CASCADE)


class FileUpliad(models.Model):
    file = models.ImageField( null=True,blank=True)
    video = models.FileField(null=True,blank=True)
    # file_thumbnail = ImageSpecField(source='file',
    #                                   processors=[ResizeToFill(150, 150)],
    #                                   format='JPEG',
    #                                   options={'quality': 60},null=True,blank=True)

    video_thumbnail = models.CharField(null=True,blank=True,max_length=1000)
    