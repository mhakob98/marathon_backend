from django.contrib import admin
from django.contrib.admin import AdminSite
from django.utils.translation import ungettext_lazy
from .models import (
    MarathonTag,
    MarathonMotivations,
    Question,
    Marathon,
    Measurement,
    MarathonPlan,
    MarathonPlanItem,
    MarathonJoinMarathonPlan,
    UserJoinMarathon,
    UserMeasurement
)
from django.contrib.auth.models import Group
from django.contrib.auth.models import User

from django.forms import ModelForm, PasswordInput
from django.contrib import admin


from marathon.admin import myems_admin_site
myems_admin_site.register(MarathonTag)
myems_admin_site.register(MarathonMotivations)
myems_admin_site.register(Question)
myems_admin_site.register(Marathon)
myems_admin_site.register(Measurement)
myems_admin_site.register(MarathonPlan)
myems_admin_site.register(MarathonPlanItem)
myems_admin_site.register(MarathonJoinMarathonPlan)
myems_admin_site.register(UserJoinMarathon)
myems_admin_site.register(UserMeasurement)




