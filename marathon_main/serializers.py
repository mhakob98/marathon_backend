from rest_framework import serializers
from utils.serializers import (
    LanguageSerializer
)
from .models import (
    Question,
    MarathonTag,
    Marathon,
    MarathonPlan,
    MarathonPlanItem,
    Measurement,
    UserMeasurement,
    MarathonMotivations,
    UserJoinMarathon,
    MarathonJoinMarathonPlan
)
from client.serializers import (
    ClientSerializer
)
class MotivationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarathonMotivations  
        fields = '__all__'


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question  
        fields = '__all__'


class MarathonTagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarathonTag  
        fields = '__all__'


class MarathonSerializer(serializers.ModelSerializer):
    coach_id = serializers.CharField(source='creator.id',read_only=True)
    coach_name = serializers.CharField(source='creator.user.first_name',read_only=True)
    coach_avatar = serializers.CharField(source='creator.avatar',read_only=True)

    class Meta:
        model = Marathon  
        fields = (
            'id',
            'start',
            'end',
            'info',
            'title',
            'creator',
            'banner',
            # 'banner_ratio',
            'language',
            'marathon_tag',
            'marathon_question',
            # 'marathon_measurement',
            'motivations',
            'coach_id',
            'coach_name',
            'coach_avatar',
            # 'weight_start' ,
            # 'weight_actual',
            # 'weight_target',
            'price_sale',
            'price_amount',
            'discriminator',
            'banners',
            'recentScore',
            'bookmark',
            'questions'
            )


class MarathonPlanSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarathonPlan  
        fields = '__all__'


class MarathonPlanItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarathonPlanItem  
        fields = '__all__'


class MeasurementSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Measurement  
        fields = '__all__'


class UserMeasurementSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserMeasurement  
        fields = '__all__'

class MarathonByIdSerializer(serializers.ModelSerializer):
    coach_id = serializers.CharField(source='creator.id',read_only=True)
    coach_name = serializers.CharField(source='creator.user.first_name',read_only=True)
    coach_avatar = serializers.CharField(source='creator.avatar',read_only=True)
    language = LanguageSerializer(read_only=True,many=True)
    marathon_question = QuestionSerializer(read_only=True,many=True)
    # marathon_measurement = MeasurementSerializer(read_only=True,many=True)
    motivations = MotivationSerializer(read_only=True,many=True)
    class Meta:
        model = Marathon  
        fields = (
            'id',
            'start',
            'end',
            'info',
            'title',
            'creator',
            'banner',
            # 'banner_ratio',
            'marathon_tag',
            'marathon_question',
            # 'marathon_measurement',
            'motivations',
            'coach_id',
            'coach_name',
            'coach_avatar',
            # 'weight_start' ,
            # 'weight_actual',
            # 'weight_target',
            'price_sale',
            'price_amount',
            'language',
            'discriminator',
            'banners',
            'recentScore',
            'bookmark',
            'questions'
            )

class UserJoinMarathonSerializer(serializers.HyperlinkedModelSerializer):
    user_info = ClientSerializer(read_only=True,source="user")
    class Meta:
        model = UserJoinMarathon
        fields = (
            'id',
            'user',
            'marathon_join_marathon_plan',
            'date',
            'user_info'
            ) 

class MarathonJoinMarathonPlanSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarathonJoinMarathonPlan
        fields = (
            'id' ,
            'marathon'  ,
            'Marathon_plan' ,
            'price' ,
            'participant_count' ,
            'marathon_plan_item' ,
            ) 
