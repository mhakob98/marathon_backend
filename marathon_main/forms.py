from django import forms

class UploadFileForm(forms.Form):
    file = forms.FileField(required=False)
    video = forms.FileField(required=False)
    