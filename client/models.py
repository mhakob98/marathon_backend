from django.db import models
from utils.models import Metric,Language
from django.contrib.auth.models import User
from career.models import (
    Certificates,
    Education,
    Experience,
    Speciality
)

from django.utils.text import slugify

# Create your models here.
class Client(models.Model):
    id = models.AutoField(primary_key=True)
    # last_name = models.CharField(max_length=100)
    # first_name = models.CharField(max_length=100)
    user = models.OneToOneField(User,on_delete=models.CASCADE, blank=True, related_name='user_client',null=True)
    google_id = models.TextField(null=True)
    facebook_id = models.TextField(null=True)
    instagram_id = models.TextField(null=True)
    vk_id = models.TextField(null=True)
    email_validation_token = models.TextField(null=True)
    device_token = models.TextField(null=True)
    device_os = models.CharField(max_length=100,null=True)
    phone_number = models.CharField(max_length=100,null=True)
    target_wight = models.IntegerField(null=True)
    crated_at = models.DateTimeField(auto_now_add=True)
    country_code = models.IntegerField(null=True)
    cover_letter = models.CharField(max_length=255,null=True)
    metric = models.ForeignKey(Metric,models.CASCADE)
    ui_language = models.ForeignKey(Language,models.CASCADE)
    stripe_account = models.IntegerField(null=True)
    avatar = models.CharField(max_length=1000,null=True,blank=True)
    status = models.TextField(null=True,blank=True)
    is_faworit = models.BooleanField(null=True,blank=True)
    language = models.ManyToManyField(Language,related_name='language_all_client',null=True,blank=True)
    certificates = models.ManyToManyField(Certificates,null=True,blank=True)
    education = models.ManyToManyField(Education,null=True,blank=True)
    experience = models.ManyToManyField(Experience,related_name="experience_list_client", null=True,blank=True)
    speciality = models.ManyToManyField(Speciality,null=True,blank=True)
    location = models.CharField(max_length=1000,null=True,blank=True)
    facebook = models.CharField(max_length=1000,null=True,blank=True)
    youtube = models.CharField(max_length=1000,null=True,blank=True)
    instagram = models.CharField(max_length=1000,null=True,blank=True)
    linkedin = models.CharField(max_length=1000,null=True,blank=True)
    cover = models.CharField(max_length=1000,null=True,blank=True)
    slug = models.SlugField('slug_link', max_length=255,null=True,blank=True)


    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name
 
    def get_fields(self):
        return dict({
            "id": self.id,
            'google_id' : self.google_id,
            'facebook_id' : self.facebook_id,
            'instagram_id' : self.instagram_id,
            'vk_id' : self.vk_id,
            'device_token' : self.device_token,
            'device_os' : self.device_os,
            'crated_at' : self.crated_at,
            'country_code' : self.country_code,
            'cover_letter' : self.cover_letter,
            # 'metric' = models.ForeignKey(Metric,models.CASCADE)
            # 'ui_language' = models.ForeignKey(Language,models.CASCADE)
            'stripe_account' : self.stripe_account,
            'avatar' : self.avatar,
            'cover' : self.cover,
            'is_faworit':self.is_faworit

        })

    def get_thumbnail_url(self):
        print(self.url)
        return self.url

    


    def save(self, *args, **kwargs):
        super(Client, self).save(*args, **kwargs)  # Save your model in order to get the id
        if not self.slug:
            self.slug = slugify(11092668+self.id)
            self.save()
            

class ClientBank(models.Model):
    id = models.AutoField(primary_key=True)
    client = models.ForeignKey(Client,models.CASCADE)
    bank = models.TextField(null=True,blank=True)