
from django.urls import path,include
from client import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'user', views.UserViewSet)
router.register(r'bank', views.ClientBankViewSet)


urlpatterns = [
    path('user-login',views.login),
    path('', include(router.urls)),
    path('get/me/',views.get_me)
]

