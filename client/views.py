from django.shortcuts import render
from rest_framework import viewsets
from .permissions import IsClient
from rest_framework.decorators import api_view,permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.settings import api_settings
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import User
from django.db.models import Q

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
from rest_framework_jwt.settings import api_settings
from django.http import JsonResponse
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
from django.contrib.auth import authenticate, get_user_model
from rest_framework import exceptions
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.generics import GenericAPIView, RetrieveUpdateAPIView
from rest_framework.response import Response
from rest_framework import status
from django.core.mail import send_mail
from feed.models import Follower
from django_filters import rest_framework as filters
import django_filters

from .models import (
    Client,
    ClientBank
)

from coach.models import Coach
from coach.serializers import CoachSerializer

from feed.serializers import FollowerSerializerLoc
from .serializers import (
    ClientSerializer,
    ClientBankSerializer,
    PasswordResetSerializer
)

# Create your views here.
class ClientFilter(django_filters.FilterSet):
    class Meta:
        model = Client
        fields = ["slug"]

@api_view(['POST'])
def login(request):
    print("ASdaskdjkalsjdasdkljsakdjsakldjksaldjklsj")
    username = request.data.get('username', None)
    password = request.data.get('password', None)
    print(username,password)
    if not username or not password:
        raise exceptions.AuthenticationFailed(('No credentials provided.'))

    credentials = {
        get_user_model().USERNAME_FIELD: username,
        'password': password
    }
    role = 'client'

    user = authenticate(**credentials)
    if user is None:
        raise exceptions.AuthenticationFailed(('Invalid username/password.'))

    if not user.is_active:
        raise exceptions.AuthenticationFailed(('User inactive or deleted.'))

    try:
        client = Client.objects.get(user=user)
    except:
        try:
            client = Coach.objects.get(user=user)
            role = 'coach'
        except:
            raise exceptions.AuthenticationFailed(('Invalid username/password.' ))

    





    refresh = RefreshToken.for_user(user)
    return JsonResponse({
        'refresh': str(refresh),
        'access': str(refresh.access_token),
        'role' : role
    })

@api_view(['GET'])
@permission_classes((IsAuthenticated,IsClient))
def get_me(request):
    try:
        user = Client.objects.get(user=request.user)
        client_info = ClientSerializer(
            instance=user, context={'request': request})

    except ObjectDoesNotExist:
        return JsonResponse({
            'message': 'User not found',
        }, status=404)

    return JsonResponse({
            'message': 'ok',
            'data': client_info.data
        }, status=200)

class PasswordResetView(GenericAPIView):
    """
    Calls Django Auth PasswordResetForm save method.
    Accepts the following POST parameters: email
    Returns the success/fail message.
    """
    serializer_class = PasswordResetSerializer
    # permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        # Create a serializer with request.data
        email = request.data.get('email', None)
        # encoded_token = jwt.encode({'email': email}, "quiz", algorithm='HS256')

        # print(email,'-------------',User.objects.get(email=email))
        try:
            user = User.objects.get(email=email)
            print(user,'--------+++++++++++++++-----')


        except:
            return Response(
                {"detail": "email wrong."},
                status=status.HTTP_400_BAD_REQUEST
            )

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        send_mail(
            subject='reset password',
            message='hello',
            html_message='<a href="http://localhost:4200/feed?token=%s">click here to change password!</a>' % (
                token),
            from_email="vano.varderesyan94@gmail.com",
            recipient_list=[email]
        )
        # Return the success message with OK HTTP status
        return Response(
            {"detail": "Password reset e-mail has been sent."},
            status=status.HTTP_200_OK
        )

@api_view(['POST'])
def resetConfirm(request):
    token = request.data.get('token', None)
    password = request.data.get('password', None)
    if not token or not password:
        raise exceptions.AuthenticationFailed(('No credentials provided.'))
    email = jwt_decode_handler(token)
    accont = User.objects.get(email=email['email'])
    accont.set_password(password)
    accont.save()
    return Response(
        {"detail": "Password has been reset with the new password."},
        status=status.HTTP_200_OK
    )

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (IsAuthenticated,IsClient)
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    filterset_class = ClientFilter

    def perform_destroy(self, instance):
        user = instance.user
        instance.delete()
        user.delete()


class ClientBankViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (IsAuthenticated,IsClient,)
    queryset = ClientBank.objects.all()
    serializer_class = ClientBankSerializer


def get_user_id_list(list):

    client_id = []
    coach_id = []
    for target_list in list:
        if target_list['whom_user']:
            client_id.append(target_list['whom_user'])
        
        if target_list['whom_coach']:
            coach_id.append(target_list['whom_coach'])

    return {
        "coach" : coach_id,
        "client" : client_id 
    }

@api_view(['GET'])
def get_all_user_familiar(request,user_id):
    user_familiar = Follower.objects.filter(Q(Q(who_user__user_id=user_id) & ~Q(who_user__user_id=None)) |Q(Q(who_coach__user_id=user_id) & ~Q(who_coach__user_id=None)))
    client_id = []
    coach_id = []
    familiar_id_client = []
    familiar_id_coach = []

    for  i  in user_familiar:
        if  i.whom_user:
            client_follower = Follower.objects.filter(Q(Q(who_user__user_id=i.whom_user.user_id) & ~Q(who_user__user_id=None))).values('whom_user','whom_coach')
            # client.append(ClientSerializer(
            # instance=client_follower,  many=True,context={'request': request}).data)
            client_id.extend(get_user_id_list(client_follower)['client'])
            coach_id.extend(get_user_id_list(client_follower)['coach'])
            familiar_id_client.append(i.whom_user.id)
        if i.whom_coach:
            choach_follower = Follower.objects.filter(Q(Q(who_coach__user_id=user_id) & ~Q(who_coach__user_id=None))).values('whom_user','whom_coach')

            client_id.extend(get_user_id_list(choach_follower)['client'])
            coach_id.extend(get_user_id_list(choach_follower)['coach'])
            familiar_id_coach.append(i.whom_coach.id)
    
    print(familiar_id_coach,'-----------',familiar_id_client)
    is_faworit_client =  Client.objects.filter(Q(Q(is_faworit=True) |Q( id__in=list(set(client_id))))).exclude(user_id=user_id).exclude(id__in=familiar_id_client)
    is_faworit_coach =  Coach.objects.filter(Q(Q(is_faworit=True) |Q( id__in=list(set(coach_id))))).exclude(user_id=user_id).exclude(id__in=familiar_id_coach)

    return Response({
            "client": ClientSerializer(instance=is_faworit_client,  many=True,context={'request': request}).data[:15],
            "coach" : CoachSerializer(instance=is_faworit_coach,  many=True,context={'request': request}).data[:15]},
            status=status.HTTP_200_OK)
