from rest_framework import permissions
from rest_framework_simplejwt.views import TokenViewBase
from .models import Client
from django.core.exceptions import ObjectDoesNotExist

class IsClient(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        try:
            client = Client.objects.get(user=request.user)
            return True
        except ObjectDoesNotExist:
            return False
    
