from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.db import transaction
from django.db.models import Q

from .models import (
    Client,
    ClientBank
)

from career.serializers import (
    CertificatesSerializer,
    EducationSerializer,
    ExperienceSerializer,
    SpecialitySerializer
)

from career.models import (
    Certificates,
    Education,
    Experience,
    Speciality
)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(
        style={'input_type': 'password'}, required=False
    )

    email = serializers.EmailField(required=False)
    class Meta:    
        model = User
        fields = (
            'id',
            'email',
            'password',
            'first_name',
            'last_name',
            
        )

class ClientSerializer(serializers.HyperlinkedModelSerializer):
   
    user = UserSerializer()

    certificates = CertificatesSerializer(many=True,required=False)
    education = EducationSerializer(many=True,required=False)
    experience = ExperienceSerializer(many=True,required=False)
    is_follower = serializers.SerializerMethodField()
    is_follower_id = serializers.SerializerMethodField()

    class Meta:
        model = Client  
        fields = (
            'id',
            'url',
            'user',
            'google_id',
            'ui_language',
            'avatar',
            'metric',
            'certificates',
            'status',
            'speciality',
            'education',
            'experience',
            'language',
            'location',
            'facebook',
            'youtube',
            'instagram',
            'linkedin',
            'is_follower',
            'cover',
            'is_follower_id',
            'is_faworit',
            'slug'

        )
    def create(self, validated_data):

        certificates = None
        educationes = None
        experiences = None
        langs = None
        specialityes = None
        
        if 'certificates' in validated_data:
            certificates = validated_data['certificates']
            del validated_data['certificates']

        if 'education' in validated_data:
            educationes = validated_data['education']
            del validated_data['education']
        
        if 'experience' in validated_data:
            experiences = validated_data['experience']
            del validated_data['experience'] 
       
        if 'speciality' in validated_data:
            specialityes = validated_data['speciality']
            del validated_data['speciality']

        if 'language' in validated_data:
            langs = validated_data['language']
            del validated_data['language']

        try:

            validated_data['user'] = User.objects.create(
                first_name=validated_data.get('user')['first_name'],
                last_name=validated_data.get('user')['last_name'],
                email=validated_data.get('user')['email'],
                password=make_password(validated_data.get('user')['password']),
                username=validated_data.get('user')['email']

            )

        except IntegrityError as e:
            raise serializers.ValidationError(e)

        instance = Client.objects.create(**validated_data)
        
        if certificates:
            for certificate in certificates:
                ce = Certificates.objects.create(**certificate)
                instance.certificates.add(ce)

        if educationes:
            for educatione in educationes:
                ed = Education.objects.create(**educatione)
                instance.education.add(ed)
        if experiences:
            for experience in experiences:
                ex = Experience.objects.create(**experience)
                instance.experience.add(ex)
        
        if langs:
            instance.language.set(langs)

        if specialityes:
            instance.speciality.set(specialityes)
            
        instance.save()
        return instance    


    @transaction.atomic
    def update(self, instance, validated_data):

        print(validated_data.get('user')['first_name'])
        User.objects.filter(id=instance.user.id).update(first_name=validated_data.get('user')['first_name'],
                                                        last_name=validated_data.get('user')['last_name'])
        instance.__dict__.update(**validated_data)
        certificates = None
        educationes = None
        experiences = None
        langs = None
        specialityes = None

    
        
        if 'certificates' in validated_data:
            certificates = validated_data['certificates']
            del validated_data['certificates']

        if 'education' in validated_data:
            educationes = validated_data['education']
            del validated_data['education']
        
        if 'experience' in validated_data:
            experiences = validated_data['experience']
            del validated_data['experience'] 


               
        if 'speciality' in validated_data:
            specialityes = validated_data['speciality']
            del validated_data['speciality']

        if 'language' in validated_data:
            langs = validated_data['language']
            del validated_data['language']

        if certificates:
            instance.certificates.clear()
            for certificate in certificates:
                ce = Certificates.objects.create(**certificate)
                instance.certificates.add(ce)

        if educationes:
            instance.education.clear()
            for educatione in educationes:
                ed = Education.objects.create(**educatione)
                instance.education.add(ed)
        if experiences:
            instance.experience.clear()
            for experience in experiences:
                ex = Experience.objects.create(**experience)
                instance.experience.add(ex)

    
        if langs:
            instance.language.set(langs)

        if specialityes:
            instance.speciality.set(specialityes)


        instance.save()
        return instance

    def get_is_follower(self, obj):
        request = self.context['request']
        user = request.user
        if obj.whom_user_follower.filter(Q(Q(who_user__user_id=user.id) & ~Q(who_user__user_id=None)) |Q(Q(who_coach__user_id=user.id) & ~Q(who_coach__user_id=None))).count() > 0  :
            return True
        else :
            return False

    def get_is_follower_id(self, obj):
        request = self.context['request']
        user = request.user
        res = obj.whom_user_follower.filter(Q(Q(who_user__user_id=user.id) & ~Q(who_user__user_id=None)) |Q(Q(who_coach__user_id=user.id) & ~Q(who_coach__user_id=None)))
        
        if res.count() > 0  :
            print(res,'-----------')

            return res[0].id
        else :
            return False

class ClientBankSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ClientBank
        fields = '__all__'


class PasswordResetSerializer(serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """
    email = serializers.EmailField()