from rest_framework import serializers

from utils.models import (
    Language,
    Metric,
    Unit,
    Country,
    Locality,
    BodyPictures
)


class LanguageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Language  
        fields = '__all__'



class MetricSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Metric  
        fields = '__all__'


class UnitSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Unit  
        fields = '__all__'


class BodyPicturesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BodyPictures  
        fields = (
            'id', 
            'at', 
            'weight', 
            'sizes', 
            'file_front', 
            'file_profile', 
            'file_back', 
        )


class CountrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Country  
        fields = '__all__'


class LocalitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Locality  
        fields = '__all__'
