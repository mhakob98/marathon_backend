from django.db import models
from django.core.validators import int_list_validator


class BodyPictures(models.Model):
    id = models.AutoField(primary_key=True)
    sizes = models.CharField(validators=[int_list_validator(sep=',',message="The string should contain only numbers separated by comma",code='invalid')], max_length=255)
    at = models.DateTimeField(auto_now_add=True)
    weight = models.IntegerField(null=True)
    file_front = models.CharField(max_length=1000,null=True,blank=True)
    file_profile = models.CharField(max_length=1000,null=True,blank=True)
    file_back = models.CharField(max_length=1000,null=True,blank=True)


class Language(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255)
    flag = models.CharField(max_length=1000,null=True,blank=True)

    def __str__(self):
        return self.name 


class Metric(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name 



class Unit(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name 


class Country(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    lat = models.FloatField(null=True)
    lng = models.FloatField(null=True)
    def __str__(self):
        return self.name 

class Locality(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    lat = models.FloatField(null=True)
    lng = models.FloatField(null=True)
    coountry = models.ForeignKey(Country,on_delete=models.CASCADE)
    def __str__(self):
        return self.name 
