from django.contrib import admin
from django.contrib.admin import AdminSite
from django.utils.translation import ungettext_lazy
from .models import (
    Country,
    Language,
    Locality,
    Metric,
    Unit,
    BodyPictures
)
from django.contrib.auth.models import Group
from django.contrib.auth.models import User

from django.forms import ModelForm, PasswordInput
from django.contrib import admin


from marathon.admin import myems_admin_site
myems_admin_site.register(Country)
myems_admin_site.register(Language)
myems_admin_site.register(Locality)
myems_admin_site.register(Metric)
myems_admin_site.register(Unit)
myems_admin_site.register(BodyPictures)






