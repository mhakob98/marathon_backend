from django.shortcuts import render
from rest_framework import viewsets
from utils.permissions import ReadOnly,IsCoach
from utils.serializers import (
    LanguageSerializer,
    MetricSerializer,
    UnitSerializer,
    CountrySerializer,
    LocalitySerializer,
    BodyPicturesSerializer,
)
from utils.models import (
    Language,
    Metric,
    Unit,
    Country,
    Locality,
    BodyPictures,
)

# Create your views here.
class LanguageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (IsCoach,)
    queryset = Language.objects.all()
    serializer_class = LanguageSerializer

class BodyPicturesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (IsCoach,)
    queryset = BodyPictures.objects.all()
    serializer_class = BodyPicturesSerializer


class MetricViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    queryset = Metric.objects.all()
    serializer_class = MetricSerializer


class UnitViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer

class CountryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    queryset = Country.objects.all()
    serializer_class = CountrySerializer

class LocalityViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """    # Feedback,

    # permission_classes = (ReadOnly,)
    queryset = Locality.objects.all()
    serializer_class = LocalitySerializer