from django.urls import path,include
from utils import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'language', views.LanguageViewSet)
router.register(r'metric', views.MetricViewSet)
router.register(r'unit',views.UnitViewSet)
router.register(r'body-pictures',views.BodyPicturesViewSet)
router.register(r'location-locality',views.LocalityViewSet)
router.register(r'location-country',views.CountryViewSet)


urlpatterns = [
    path('', include(router.urls))
]
