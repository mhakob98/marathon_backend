from rest_framework import permissions
from rest_framework.authentication import (
    get_authorization_header
)
from rest_framework_jwt.settings import api_settings

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
from rest_framework_simplejwt import authentication
class ReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        print(request.method in permissions.SAFE_METHODS)
        return request.method in permissions.SAFE_METHODS

class IsCoach(permissions.BasePermission):
    def has_permission(self,request,view):
        auth = get_authorization_header(request).split()
        if len(auth) > 0:
            payload = jwt_decode_handler(auth[1])
            print(payload)
            if payload['role'] == 'coach':
                return True
            else:
                return False
        else:
            return False

