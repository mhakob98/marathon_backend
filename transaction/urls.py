
from django.urls import path,include
from .views import (pay,checkTransaction,TransactionViewSet)
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'all', TransactionViewSet)
# router.register(r'training' , views.TrainingViewSet)

urlpatterns = [
    path('', include(router.urls)),

    path('pay', pay),
    path('check', checkTransaction)
    
]

