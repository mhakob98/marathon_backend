# Generated by Django 2.1.13 on 2020-07-28 12:33

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('transaction', '0022_auto_20200723_0846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2020, 7, 28, 12, 33, 21, 417146, tzinfo=utc), null=True),
        ),
    ]
