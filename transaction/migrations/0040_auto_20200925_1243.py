# Generated by Django 2.1.13 on 2020-09-25 12:43

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('transaction', '0039_auto_20200924_1030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2020, 9, 25, 12, 43, 32, 806185, tzinfo=utc), null=True),
        ),
    ]
