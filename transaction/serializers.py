
from rest_framework import serializers
from .models import Transaction




class PaySerializer(serializers.Serializer):
    money = serializers.FloatField()

class CheckSerializer(serializers.Serializer):
    id = serializers.IntegerField()



class TransactionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Transaction  
        fields = ('url','id','money','payment_id',"date","status","coach",'client','marathon')