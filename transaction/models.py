from django.db import models
from client.models import Client
from coach.models import Coach
from marathon_main.models import Marathon
import datetime
# Create your models here.

from django.utils.timezone import now

class Transaction(models.Model):
    id = models.AutoField(primary_key=True)
    money = models.FloatField(null=True)
    payment_id = models.CharField(null=True,max_length=1000)
    date = models.DateTimeField(null=True,blank=True,default=now())
    status = models.CharField(max_length=1000,null=True,blank=True)
    coach = models.ForeignKey(Coach,null=True,on_delete=models.CASCADE,blank=False)
    client = models.ForeignKey(Client,null=True,on_delete=models.CASCADE,blank=False)
    marathon = models.ForeignKey(Marathon,null=True,on_delete=models.CASCADE,blank=False)


    def __str__(self):
        return str(self.id)



