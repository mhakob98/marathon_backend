from django.shortcuts import render

# Create your views here.
from .serializers import(PaySerializer,CheckSerializer,TransactionSerializer)
from rest_framework.decorators import api_view,permission_classes
        
from django.http import JsonResponse
from rest_framework.permissions import IsAuthenticated
from client.permissions import IsClient
# @method_decorator(login_required)
import requests
import json
from rest_framework.request import Request
from rest_framework.test import APIRequestFactory
from rest_framework import viewsets

from .models import Transaction

factory = APIRequestFactory()
_request = factory.get('/')


serializer_context = {
    'request': Request(_request),
}


class TransactionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer



@api_view(['POST'])
# @permission_classes((IsAuthenticated,IsClient))
def pay(request, format=None):
        serializer = TransactionSerializer(data=request.data,context={'request': _request})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        print(serializer.data)
        ploads = {
                    'userName':"marathon_test",
                    'password':"6p<Azz'XCbE2Z8c>",
                    "amount":request.data['money'],
                    "orderNumber" : serializer.data['id'],	

                    "returnUrl" : "http://annaniks.com:6262/api/transaction/check?id="+str(serializer.data['id']),
                    "language":"ru",
                    }
        r = requests.post('https://ipaytest.arca.am:8445/payment/rest/register.do',params=ploads)

        return JsonResponse({
            "url"  :r.url,
            "message" :  json.loads(r.text)
        }, status=201)



@api_view(['get'])
# @permission_classes((IsAuthenticated,IsClient))
def checkTransaction(request, format=None):
        # serializer = CheckSerializer(data=request.data)
        # serializer.is_valid(raise_exception=True)
    order_id = request.GET.get('orderId')
    id = request.GET.get('id')

    ploads = {
                'userName':"marathon_test",
                'password':"6p<Azz'XCbE2Z8c>",
                "orderId":order_id
           
                }
    r = requests.post('https://ipaytest.arca.am:8445/payment/rest/getOrderStatus.do',params=ploads)
    tr = Transaction.objects.get(pk=id)
    tr.payment_id = order_id
    tr.save()
    return JsonResponse({
        "message" :  json.loads(r.text)
    }, status=201)
