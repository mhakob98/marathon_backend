"""marathon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including anotunchangedher URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path,include
from coach import views as CoachViews
from client import views as ClientViews
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from rest_framework.schemas import get_schema_view
from rest_framework.documentation import include_docs_urls 
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
schema_view = get_schema_view(title='Blog API')
from .admin import  myems_admin_site
from client.views import(
    PasswordResetView,
    resetConfirm,
    get_all_user_familiar
)
from marathon_main.views import upload_file
urlpatterns = [
    path('jet', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    path('', myems_admin_site.urls),
    # path('', myems_admin_site.urls),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),

    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/coach-login/',CoachViews.login),
    path('api/coach-password/',CoachViews.changePassword),
    path('api/client-login/',ClientViews.login),
    path('api/client-password/',CoachViews.changePassword),
    path('api/utils/',include('utils.urls')),
    path('api/feed/',include('feed.urls')),
    path('api/ration/',include('ration.urls')),
    path('api/training/',include('training.urls')),
    path('api/transaction/',include('transaction.urls')),
    path('api/career/',include('career.urls')),
    path('api/coach/',include('coach.urls')),
    path('api/client/',include('client.urls')),
    path('api/marathon-main/',include('marathon_main.urls')),
    path('api/marathon-calendar/',include('calendar_marathon.urls')),
    path('auth/', include('rest_framework_social_oauth2.urls')),
    # path('docs/', include_docs_urls(title='Blog API')), 
    path('schema/', schema_view),
    path('upload-file/', upload_file),
    path('api/password/reset/', PasswordResetView.as_view(),
        name='rest_password_reset'),
    path('api/password/reset/confirm/', resetConfirm),
    path('api/user/familiar/<int:user_id>', get_all_user_familiar),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



