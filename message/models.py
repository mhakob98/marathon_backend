from django.db import models

# Create your models here.
from coach.models import Coach
from client.models import Client

class MessageTopic(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    marathon = models.IntegerField()

class MessageParticipant(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ManyToManyField(Client)
    topic = models.IntegerField()
    coach = models.ManyToManyField(Coach)
    isCreator = models.BooleanField()

class MessageItem(models.Model):
    id = models.AutoField(primary_key=True)
    timeStamp = models.DateTimeField()
    message_text = models.TextField()
    topic = models.ManyToManyField(MessageTopic)

class MessageItemSeen(models.Model):
    id = models.AutoField(primary_key=True)
    message_item = models.ManyToManyField(MessageItem)
    message_participant = models.ManyToManyField(MessageParticipant)
    timeStamp = models.DateTimeField()