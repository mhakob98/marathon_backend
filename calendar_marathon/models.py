from django.db import models
from training.models import Training
from ration.models import Ration,Metric
from client.models import Client
from coach.models import Coach
# Create your models here.


class CalendarCategories(models.Model):
    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    discriminator = models.IntegerField(null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    color = models.CharField(null=True,blank=True, max_length=250)
    thumb = models.IntegerField(null=True,blank=True)
    counter = models.IntegerField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class MarathonCalendarScheduler(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100,null=True)
    banner = models.CharField(max_length=1000,null=True,blank=True)
    discriminator = models.IntegerField(null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    durationDays = models.IntegerField(null=True,blank=True)
    thumbUrl = models.TextField(null=True,blank=True)
    categories = models.ManyToManyField(CalendarCategories, null=True,blank=True)
    options = models.TextField(null=True,blank=True)
    marathon = models.ForeignKey('marathon_main.Marathon',related_name="calendar_marathon_main", null=True,blank=True ,on_delete=models.CASCADE)
    type = models.IntegerField(null=True,blank=True,default=0)
    weekStartDay = models.IntegerField(null=True,blank=True,default=0)
    bookmark = models.IntegerField(null=True,blank=True,default=0)
    recentScore = models.IntegerField(null=True,blank=True)
    metric = models.ForeignKey(Metric, related_name='calendar_metric', on_delete=models.CASCADE,null=True,blank=True)
    metrics= models.TextField(null=True,blank=True)
    valuesType = models.IntegerField(null=True,blank=True)
    valuesRaw= models.TextField(null=True,blank=True)
    valueTypes= models.TextField(null=True,blank=True)
    autoEnabled = models.IntegerField(null=True,blank=True)
    notes= models.TextField(null=True,blank=True)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)


class MarathonCalendarSchedulerItem(models.Model):
    id = models.AutoField(primary_key=True)
    marathon_calendar_shceduler = models.ForeignKey(MarathonCalendarScheduler,on_delete=models.CASCADE)
    day = models.IntegerField(null=True,blank=True)
    freeNutrients = models.IntegerField(null=True,blank=True)
    freeTrainings = models.IntegerField(null=True,blank=True)
    metric = models.ForeignKey(Metric, related_name='calendar_item_metric', null=True,blank=True,on_delete=models.CASCADE)
    metrics= models.TextField(null=True,blank=True)
    valuesType = models.IntegerField(null=True,blank=True)
    valuesRaw= models.TextField(null=True,blank=True)
    valueTypes= models.TextField(null=True,blank=True)
    type = models.IntegerField(null=True,blank=True)
    autoEnabled = models.IntegerField(null=True,blank=True)
    notes= models.TextField(null=True,blank=True)
    containerParts = models.TextField(null=True,blank=True)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)

    def __str__(self):
        return 'Day - '+ str(self.day)


class MarathonCalendarSchedulerItemTraining(models.Model):
    id = models.AutoField(primary_key=True)
    marathon_calendar_shceduler_item = models.ForeignKey(MarathonCalendarSchedulerItem,on_delete=models.CASCADE)
    order = models.IntegerField()
    training = models.OneToOneField(Training,on_delete=models.CASCADE)

class MarathonCalendarSchedulerItemRation(models.Model):
    id = models.AutoField(primary_key=True)
    marathon_calendar_shceduler_item = models.ForeignKey(MarathonCalendarSchedulerItem,on_delete=models.CASCADE)
    order = models.IntegerField()
    ration = models.OneToOneField(Ration,on_delete=models.CASCADE)

class UserMarathonCalendarRation(models.Model):
    id = models.AutoField(primary_key=True)
    marathon_calendar_shceduler_item_ration = models.ForeignKey(MarathonCalendarSchedulerItemRation,on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    take = models.BooleanField()
    user = models.ForeignKey(Client,on_delete=models.CASCADE)

class UserMarathonCalendarTraining(models.Model):
    id = models.AutoField(primary_key=True)
    marathon_calendar_shceduler_item_training = models.ForeignKey(MarathonCalendarSchedulerItemTraining,on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    take = models.BooleanField()
    user = models.ForeignKey(Client,on_delete=models.CASCADE)
