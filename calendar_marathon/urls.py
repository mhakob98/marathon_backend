
from django.urls import path,include
from calendar_marathon import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'marathon-calendar-scheduler', views.MarathonCalendarSchedulerViewSet)
router.register(r'marathon-calendar-scheduler-item', views.MarathonCalendarSchedulerItemViewSet)
router.register(r'marathon-calendar-scheduler-item-ration', views.MarathonCalendarSchedulerItemRationViewSet)
router.register(r'marathon-calendar-scheduler-item-training', views.MarathonCalendarSchedulerItemTrainingViewSet)
router.register(r'calendar-category', views.CalendarCategoriesViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path(r'calendar-duplicate', views.duplicate_calendar)
]

