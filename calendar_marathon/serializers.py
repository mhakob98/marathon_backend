from rest_framework import serializers

from .models import (
   MarathonCalendarScheduler,
   MarathonCalendarSchedulerItem,
   CalendarCategories,
   MarathonCalendarSchedulerItem,
   MarathonCalendarSchedulerItemRation,
   MarathonCalendarSchedulerItemTraining,
)

class CalendarCategoriesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CalendarCategories  
        fields = '__all__'


class MarathonCalendarSchedulerItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarathonCalendarSchedulerItem  
        fields = '__all__'


class MarathonCalendarSchedulerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarathonCalendarScheduler  
        fields = '__all__'

class MarathonCalendarSchedulerItemRationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarathonCalendarSchedulerItemRation  
        fields = '__all__'

class MarathonCalendarSchedulerItemTrainingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MarathonCalendarSchedulerItemTraining  
        fields = '__all__'