from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import api_view
from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response
from django_filters import rest_framework as filters
import django_filters

from .models import (
    MarathonCalendarScheduler,
    MarathonCalendarSchedulerItem,
    CalendarCategories,
    MarathonCalendarSchedulerItem,
    MarathonCalendarSchedulerItemTraining,
    MarathonCalendarSchedulerItemRation,
)

from .serializers import (
    MarathonCalendarSchedulerSerializer,
    MarathonCalendarSchedulerItemSerializer,
    CalendarCategoriesSerializer,
    MarathonCalendarSchedulerItemSerializer,
    MarathonCalendarSchedulerItemTrainingSerializer,
    MarathonCalendarSchedulerItemRationSerializer,
)

# Create your views here.

class ShcedulerRationFilter(django_filters.FilterSet):
    marathon_scheduler = django_filters.NumberFilter(field_name='marathon_calendar_shceduler_item__marathon_calendar_shceduler', lookup_expr='exact')

    class Meta:
        model = MarathonCalendarSchedulerItemRation
        fields = ["marathon_scheduler"]

class ShcedulerTrainingFilter(django_filters.FilterSet):
    marathon_scheduler = django_filters.NumberFilter(field_name='marathon_calendar_shceduler_item__marathon_calendar_shceduler', lookup_expr='exact')

    class Meta:
        model = MarathonCalendarSchedulerItemTraining
        fields = ["marathon_scheduler"]


class ShcedulerFilter(django_filters.FilterSet):
    class Meta:
        model = MarathonCalendarSchedulerItem
        fields = ["marathon_calendar_shceduler","creator","creator_client"]


class CoachFilter(django_filters.FilterSet):
    discriminator = django_filters.RangeFilter()

    class Meta:
        model = MarathonCalendarScheduler
        fields = ["creator","discriminator"]
# Create your views here.


class CalendarCategoriesFilter(django_filters.FilterSet):
    discriminator = django_filters.RangeFilter()
    class Meta:
        model = CalendarCategories
        fields = ["discriminator","creator_client","creator"]

class MarathonCalendarSchedulerItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = [permissions.IsAuthenticated]
    queryset = MarathonCalendarSchedulerItem.objects.all()
    serializer_class = MarathonCalendarSchedulerItemSerializer


class CalendarCategoriesViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = [permissions.IsAuthenticated]
    queryset = CalendarCategories.objects.all()
    serializer_class = CalendarCategoriesSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = CalendarCategoriesFilter


class MarathonCalendarSchedulerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = [permissions.IsAuthenticated]
    queryset = MarathonCalendarScheduler.objects.all()
    serializer_class = MarathonCalendarSchedulerSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = CoachFilter

class MarathonCalendarSchedulerItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = [permissions.IsAuthenticated]
    queryset = MarathonCalendarSchedulerItem.objects.all()
    serializer_class = MarathonCalendarSchedulerItemSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = ShcedulerFilter

class MarathonCalendarSchedulerItemTrainingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = [permissions.IsAuthenticated]
    queryset = MarathonCalendarSchedulerItemTraining.objects.all()
    serializer_class = MarathonCalendarSchedulerItemTrainingSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = ShcedulerTrainingFilter

class MarathonCalendarSchedulerItemRationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = [permissions.IsAuthenticated]
    queryset = MarathonCalendarSchedulerItemRation.objects.all()
    serializer_class = MarathonCalendarSchedulerItemRationSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = ShcedulerRationFilter

@api_view(['POST'])
def duplicate_calendar(request):
    if request.method == 'POST':
        calendar = MarathonCalendarScheduler.objects.get(pk=request.query_params.get('id'))
        calendar.pk = None
        calendar.save()
        return Response({"status":"ok"}, status=status.HTTP_201_CREATED)
