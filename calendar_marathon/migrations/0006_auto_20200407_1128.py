# Generated by Django 2.1.13 on 2020-04-07 11:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calendar_marathon', '0005_marathoncalendarscheduler_creator'),
    ]

    operations = [
        migrations.AlterField(
            model_name='marathoncalendarscheduler',
            name='banner',
            field=models.CharField(blank=True, max_length=1000, null=True),
        ),
    ]
