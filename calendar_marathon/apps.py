from django.apps import AppConfig


class CalendarMarathonConfig(AppConfig):
    name = 'calendar_marathon'
