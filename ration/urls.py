
from django.urls import path,include
from ration import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'product', views.ProductViewSet)
router.register(r'ingredient', views.IngredientViewSet)
router.register(r'nutrition',views.NutritionViewSet)
router.register(r'product-nutrition',views.ProductNutritionViewSet)
router.register(r'ration', views.RationViewSet)
router.register(r'ration-category', views.RationCategoryViewSet)
router.register(r'nutrition-type', views.NutrientTypeViewSet)
router.register(r'ingredient-category', views.IngredientCategoryViewSet)
router.register(r'metric', views.MetricViewSet)




urlpatterns = [
    path('', include(router.urls))
]

