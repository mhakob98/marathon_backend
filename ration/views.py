from rest_framework import viewsets
from .permissions import ReadOnly
from rest_framework import permissions
from .models import (
    Product,
    Ingredient,
    Ration,
    Nutrition,
    ProductNutrition,
    RationCategory,
    NutrientType,
    IngredientCategory,
    Metric
)

from .serializers import (
    ProductSerializer,
    IngredientSerializer,
    RationSerializer,
    NutritionSerializer,
    ProductNutritionSerializer,
    RationCategorySerializer,
    NutrientTypeSerializer,
    IngredientCategorySerializer,
    MetricSerializer
)
import django_filters

from django_filters import rest_framework as filters

# Create your views here.

class RationFilter(django_filters.FilterSet):
    discriminator = django_filters.RangeFilter()
    
    class Meta:
        model = Ration
        fields = ["creator","discriminator"]

class RationCategoryFilter(django_filters.FilterSet):
    discriminator = django_filters.RangeFilter()
    
    class Meta:
        model = RationCategory
        fields = ["creator","creator_client",'discriminator']


class MetricFilter(django_filters.FilterSet):
    class Meta:
        model = Metric
        fields = ["creator","creator_client"]

class IngredientFilter(django_filters.FilterSet):
    class Meta:
        model = Ingredient
        fields = ["creator","creator_client"]


class IngredientCategoryFilter(django_filters.FilterSet):
    class Meta:
        model = IngredientCategory
        fields = ["creator","creator_client"]

class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class IngredientViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = IngredientFilter


class RationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Ration.objects.all()
    serializer_class = RationSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = RationFilter

class NutritionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = [permissions.IsAuthenticated]

    queryset = Nutrition.objects.all()
    serializer_class = NutritionSerializer

class ProductNutritionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = ProductNutrition.objects.all()
    serializer_class = ProductNutritionSerializer


class RationCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = RationCategory.objects.all()
    serializer_class = RationCategorySerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = RationCategoryFilter



class NutrientTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = NutrientType.objects.all()
    serializer_class = NutrientTypeSerializer

class IngredientCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = IngredientCategory.objects.all()
    serializer_class = IngredientCategorySerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = IngredientCategoryFilter
class MetricViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Metric.objects.all()
    serializer_class = MetricSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = MetricFilter

    