from django.contrib import admin
from django.contrib.admin import AdminSite
from django.utils.translation import ungettext_lazy
from .models import (
    Ingredient,
    Nutrition,
    Product,
    Ration,
    ProductNutrition
)
from django.contrib.auth.models import Group
from django.contrib.auth.models import User

from django.forms import ModelForm, PasswordInput
from django.contrib import admin


from marathon.admin import myems_admin_site
myems_admin_site.register(Ingredient)
myems_admin_site.register(Nutrition)
myems_admin_site.register(Product)
myems_admin_site.register(Ration)
myems_admin_site.register(ProductNutrition)





