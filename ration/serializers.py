from rest_framework import serializers

from .models import (
    Product,
    Ingredient,
    Ration,
    Product,
    ProductNutrition,
    Nutrition,
    RationCategory,
    NutrientType,
    IngredientCategory,
    Metric
)

class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product  
        fields = '__all__'



class IngredientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ingredient  
        fields = '__all__'




class RationCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = RationCategory  
        fields = '__all__'


class RationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ration  
        fields = '__all__'


class NutritionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Nutrition  
        fields = '__all__'
        

class ProductNutritionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ProductNutrition  
        fields = '__all__'
        

class NutrientTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = NutrientType  
        fields = '__all__'

class IngredientCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = IngredientCategory  
        fields = '__all__'

class MetricSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Metric  
        fields = '__all__'