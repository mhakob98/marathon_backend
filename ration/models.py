from django.db import models
from coach.models import Coach
from client.models import Client
from utils.models import Unit
# from marathon_main.models import Marathon
# Create your models here.

class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    ccal = models.FloatField()
    protein = models.FloatField()
    carbonitrat = models.FloatField()
    nutreint = models.FloatField()
    image = models.CharField(max_length=1000,null=True,blank=True)



class Metric(models.Model):
    id = models.AutoField(primary_key=True)
    text = models.TextField(null=True,blank=True)
    abbr= models.TextField(null=True,blank=True)
    conventionalUnit = models.FloatField(null=True,blank=True)
    conventionalMetric = models.IntegerField(null=True,blank=True)
    energeticSize = models.FloatField(null=True,blank=True)
    custom = models.IntegerField(null=True,blank=True)
    isBase = models.BooleanField(null=True,blank=True)
    isValidated = models.BooleanField(null=True,blank=True)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,related_name="metric_creator",null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,related_name="metric_creator_client",null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)
    
    class Meta:
        verbose_name = "Metric"
        verbose_name_plural = "Metrics"

    def __str__(self):
        return str(self.text+str(self.id))

    def get_absolute_url(self):
        return reverse("Metric_detail", kwargs={"pk": self.pk})

class RationCategory(models.Model):
    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    discriminator = models.IntegerField(null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    color = models.CharField(null=True,blank=True, max_length=250)
    thumb = models.TextField(null=True,blank=True)
    counter = models.IntegerField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)
    

    class Meta:
        verbose_name = "RationCategory"
        verbose_name_plural = "RationCategorys"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("RationCategory_detail", kwargs={"pk": self.pk})

class Ration(models.Model):
    id = models.AutoField(primary_key=True)
    discriminator = models.IntegerField(null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    info = models.TextField(null=True,blank=True)
    thumb=models.TextField(null=True,blank=True)
    thumbs= models.TextField(null=True,blank=True)
    video= models.TextField(null=True,blank=True)
    recipeSteps= models.TextField(null=True,blank=True)
    partialIngredients = models.TextField(null=True,blank=True)
    categories = models.ManyToManyField(RationCategory, null=True,blank=True)
    bookmark = models.IntegerField(null=True,blank=True)
    recentScore = models.IntegerField(null=True,blank=True)
    preparationTime = models.IntegerField(null=True,blank=True)
    metric = models.ForeignKey(Metric, null=True,blank=True,related_name='ration_metric', on_delete=models.CASCADE)
    metrics= models.TextField(null=True,blank=True)
    valuesType = models.IntegerField(null=True,blank=True)
    valuesRaw= models.TextField(null=True,blank=True)
    valueTypes= models.TextField(null=True,blank=True)
    autoEnabled = models.IntegerField(null=True,blank=True)
    notes= models.TextField(null=True,blank=True)
    containerParts= models.TextField(null=True,blank=True)
    name = models.CharField(max_length=255,null=True,blank=True)
    description = models.TextField(null=True,blank=True)
    steps = models.TextField(null=True,blank=True)
    creator = models.ForeignKey(Coach, on_delete=models.CASCADE,null=True,blank=True)  
    user = models.ForeignKey(Client, on_delete=models.CASCADE,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)

class NutrientType(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.TextField(null=True,blank=True)
    energy = models.FloatField(null=True,blank=True) 
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)

    class Meta:
        verbose_name = "NutrientType"
        verbose_name_plural = "NutrientTypes"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("NutrientType_detail", kwargs={"pk": self.pk})

class IngredientCategory(models.Model):
    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    color = models.CharField(null=True,blank=True, max_length=250)
    thumb = models.TextField(null=True,blank=True)
    counter = models.IntegerField(null=True,blank=True)
    # parent = models.ForeignKey("self", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)
    

    class Meta:
        verbose_name = "IngredientCategory"
        verbose_name_plural = "IngredientCategorys"

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("IngredientCategory_detail", kwargs={"pk": self.pk})

class Ingredient(models.Model):
    id = models.AutoField(primary_key=True)
    thumb = models.TextField(null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    description = models.TextField(null=True,blank=True)
    brand = models.TextField(null=True,blank=True)
    categories = models.ManyToManyField(IngredientCategory,null=True,blank=True,verbose_name="ingredient_category")
    bookmark = models.IntegerField(null=True,blank=True)
    recentScore = models.IntegerField(null=True,blank=True)
    isValidated = models.IntegerField(null=True,blank=True)
    metric = models.ForeignKey(Metric, null=True,blank=True,related_name='ingredient_metric', on_delete=models.CASCADE)
    metrics= models.TextField(null=True,blank=True)
    valuesType = models.IntegerField(null=True,blank=True)
    valuesRaw= models.TextField(null=True,blank=True)
    valueTypes= models.TextField(null=True,blank=True)
    autoEnabled = models.IntegerField(null=True,blank=True)
    notes= models.TextField(null=True,blank=True)
    # product = models.ForeignKey(Product, on_delete=models.CASCADE)
    # unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    # quantity = models.FloatField()
    ration_category = models.ForeignKey(RationCategory, on_delete=models.CASCADE,null=True,blank=True)
    # ingredient_category = models.ForeignKey(IngredientCategory, on_delete=models.CASCADE)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)



class Nutrition(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

class ProductNutrition(models.Model):
    id = models.AutoField(primary_key=True)
    nutrition = models.ForeignKey(Nutrition, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    quantity = models.FloatField()
