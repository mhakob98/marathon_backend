
from django.urls import path,include
from training import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'body-part', views.BodyPartViewSet)
router.register(r'training' , views.TrainingViewSet)
router.register(r'muscle' , views.MuscleViewSet)
router.register(r'category' , views.CategoryViewSet)



urlpatterns = [
    path('', include(router.urls))
]

