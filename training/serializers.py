from rest_framework import serializers

from .models import (
    Training,
    BodyPart,
    Muscle,
    Category
)

class TrainingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Training  
        fields = (
            '__all__' 
        )

class BodyPartSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = BodyPart  
        fields = '__all__'


class MuscleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Muscle  
        fields = '__all__'


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category  
        fields = '__all__'