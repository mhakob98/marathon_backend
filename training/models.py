from django.db import models
from coach.models import Coach
from client.models import Client
from ration.models import Metric

class BodyPart(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Category(models.Model):

    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    discriminator = models.IntegerField(null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    color = models.CharField(null=True,blank=True, max_length=250)
    thumb = models.IntegerField(null=True,blank=True)
    counter = models.IntegerField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categorys"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Category_detail", kwargs={"pk": self.pk})

class Muscle(models.Model):
    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE,null=True,blank=True)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    picture = models.TextField(null=True,blank=True)
    postX = models.IntegerField(null=True,blank=True)
    postY = models.IntegerField(null=True,blank=True)
    isBase = models.BooleanField(null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)

    class Meta:
        verbose_name = "Muscle"
        verbose_name_plural = "Muscles"

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Muscle_detail", kwargs={"pk": self.pk})


class Training(models.Model):
    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(Coach,on_delete=models.CASCADE)
    creator_client = models.ForeignKey(Client,on_delete=models.CASCADE,null=True,blank=True)
    discriminator = models.IntegerField(null=True,blank=True)
    title = models.TextField(null=True,blank=True)
    info = models.TextField(null=True,blank=True)
    categories = models.ManyToManyField(Category,related_name="categories", null=True,blank=True  )
    equipment = models.ManyToManyField(Category,related_name="equipment",null=True,blank=True  )
    muscles = models.ManyToManyField(Muscle,null=True,blank=True  )
    parts = models.TextField(null=True,blank=True)
    level = models.IntegerField(null=True,blank=True,default=0)
    videoInfo = models.TextField(null=True,blank=True)
    videoUrl = models.TextField(null=True,blank=True)
    videoThumbUrl = models.TextField(null=True,blank=True)
    videoDurationMilliseconds = models.IntegerField(null=True,blank=True)
    videoEndMilliseconds = models.IntegerField(null=True,blank=True)
    videoStartMilliseconds = models.IntegerField(null=True,blank=True)
    videoAspectRatio = models.FloatField(null=True,blank=True)
    thumbUrl = models.TextField(null=True,blank=True)
    bookmark = models.BooleanField(null=True,blank=True)
    recentScore = models.IntegerField(null=True,blank=True)
    metric=models.ForeignKey(Metric,on_delete=models.CASCADE,null=True,blank=True)
    metrics=models.TextField(null=True,blank=True)
    valuesType=models.TextField(null=True,blank=True)
    valuesRaw=models.TextField(null=True,blank=True)
    valueTypes=models.TextField(null=True,blank=True)
    autoEnabled=models.IntegerField(null=True,blank=True)
    notes=models.TextField(null=True,blank=True)
    containerParts=models.TextField(null=True,blank=True)
    body_part = models.ManyToManyField(BodyPart,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,null=True,blank=True)