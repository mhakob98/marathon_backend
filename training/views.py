from django.shortcuts import render
from rest_framework import viewsets
from .permissions import ReadOnly
from .models import (
    Training,
    BodyPart,
    Muscle,
    Category
)

from .serializers import (
    TrainingSerializer,
    BodyPartSerializer,
    MuscleSerializer,
    CategorySerializer
)

import django_filters

from django_filters import rest_framework as filters

# Create your views here.

class TrainingFilter(django_filters.FilterSet):
    discriminator = django_filters.RangeFilter()

    class Meta:
        model = Training
        fields = ["creator","discriminator"]


class CategoryFilter(django_filters.FilterSet):
    discriminator = django_filters.RangeFilter()
    class Meta:
        model = Category
        fields = ["creator","discriminator"]


class TrainingViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Training.objects.all()
    serializer_class = TrainingSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = TrainingFilter

class BodyPartViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = BodyPart.objects.all()
    serializer_class = BodyPartSerializer


class MuscleViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Muscle.objects.all()
    serializer_class = MuscleSerializer

class CategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (ReadOnly,)
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = CategoryFilter