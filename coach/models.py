from django.db import models
from django.contrib.auth.models import User

from career.models import (
    Certificates,
    Education,
    Experience,
    Speciality
)
from utils.models import Metric, Language,Country,Locality
from django.utils.text import slugify


class CoachPlan(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    price = models.FloatField()
    participant_count = models.IntegerField()

    def __str__(self):
        return self.name

class Coach(models.Model):
    id = models.AutoField(primary_key=True)
    # last_name = models.CharField(max_length=100,null=True)
    # first_name = models.CharField(max_length=100,null=True)
    user = models.OneToOneField(User,on_delete=models.CASCADE, blank=True, related_name='coach_user')
    about = models.TextField(null=True)
    avatar = models.CharField(max_length=1000,null=True,blank=True)
    google_id = models.TextField(null=True,blank=True)
    facebook_id = models.TextField(null=True,blank=True)
    instagram_id = models.TextField(null=True,blank=True)
    vk_id = models.TextField(null=True,blank=True)
    email_validation_token = models.TextField(null=True,blank=True)
    device_token = models.TextField(null=True)
    device_os = models.CharField(max_length=100,null=True)
    phone_number = models.CharField(max_length=100,null=True,blank=True)
    target_wight = models.IntegerField(null=True,blank=True)
    crated_at = models.DateTimeField(auto_now_add=True,blank=True)
    country_code = models.IntegerField(null=True,blank=True)
    cover_letter = models.CharField(max_length=255,null=True,blank=True)
    metric = models.ManyToManyField(Metric,null=True,blank=True)
    ui_language = models.ManyToManyField(Language,related_name='languages',null=True,blank=True)
    
    language = models.ManyToManyField(Language,related_name='language_all',null=True,blank=True)
    certificates = models.ManyToManyField(Certificates,null=True,blank=True)
    education = models.ManyToManyField(Education,null=True,blank=True)
    experience = models.ManyToManyField(Experience,related_name="experience_list", null=True,blank=True)
    speciality = models.ManyToManyField(Speciality,null=True,blank=True)
    location = models.CharField(max_length=1000,null=True,blank=True)
    facebook = models.CharField(max_length=1000,null=True,blank=True)
    youtube = models.CharField(max_length=1000,null=True,blank=True)
    instagram = models.CharField(max_length=1000,null=True,blank=True)
    linkedin = models.CharField(max_length=1000,null=True,blank=True)
    is_faworit = models.BooleanField(null=True,blank=True)
    plan = models.ManyToManyField(CoachPlan)
    stripe_account = models.IntegerField(null=True,blank=True)
    status = models.TextField(null=True,blank=True)
    rate = models.IntegerField(null=True,blank=True)
    locationLocality = models.ForeignKey(Locality,on_delete=models.CASCADE,null=True,blank=True)
    locationCountry = models.ForeignKey(Country,on_delete=models.CASCADE,null=True,blank=True) 
    cover = models.CharField(max_length=1000,null=True,blank=True)
    slug = models.SlugField('slug_link', max_length=255,null=True,blank=True)

    def __str__(self):
        return str(self.user.first_name)
        
    def get_fields(self):
        return dict({
            'id':self.id,
            'first_name':self.user.first_name,
            'last_name':self.user.last_name,
            'email':self.user.email,     
            'google_id' : self.google_id,
            'facebook_id' : self.facebook_id,
            'instagram_id' : self.instagram_id,
            'vk_id' : self.vk_id,
            'device_token' : self.device_token,
            'device_os' : self.device_os,
            'crated_at' : self.crated_at,
            'country_code' : self.country_code,
            'cover_letter' : self.cover_letter,
            # metric = models.ManyToManyField(Metric)
            # ui_language = models.ManyToManyField(Language)
            # plan = models.ManyToManyField(CoachPlan)
            'stripe_account' : self.stripe_account,
            'avatar' : self.avatar,
            'cover' : self.cover,
            'is_faworit' : self.is_faworit,
            'slug' : self.slug

        })
    
    def save(self, *args, **kwargs):
        super(Coach, self).save(*args, **kwargs)  # Save your model in order to get the id
        
        if not self.slug:
            self.slug = slugify(21092668+self.id)
            self.save()

class CoachPlanSubscription(models.Model):
    id = models.AutoField(primary_key=True)
    start = models.DateTimeField()
    expire = models.DateTimeField()
    coach = models.ForeignKey(Coach,on_delete=models.CASCADE)
    coach_plan = models.ForeignKey(CoachPlan,models.CASCADE)
    stripe_transaction = models.IntegerField()

class CoachLanguage(models.Model):
    id = models.AutoField(primary_key=True)
    Coach = models.ForeignKey(Coach,models.CASCADE)
    language = models.ForeignKey(Language,models.CASCADE)


class CoachBank(models.Model):
    id = models.AutoField(primary_key=True)
    coach = models.ForeignKey(Coach,models.CASCADE)
    bank = models.TextField(null=True,blank=True)