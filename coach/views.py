from django.shortcuts import render
from rest_framework import viewsets
from .permissions import IsCoach
from rest_framework.decorators import api_view,permission_classes
from rest_framework.authentication import (
    get_authorization_header
)
from rest_framework_jwt.settings import api_settings

from django.core import serializers
import json
from rest_framework.test import APIRequestFactory
from rest_framework import viewsets
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.tokens import RefreshToken
from django.http import JsonResponse
from django.contrib.auth import authenticate, get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.core import serializers
from marathon_main.models import Marathon
# Create your views here.
from rest_framework import generics
from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework.request import Request
from rest_framework import pagination
import django_filters

factory = APIRequestFactory()
request = factory.get('/')


serializer_context = {
    'request': Request(request),
}


from .models import (
    Coach,
    CoachPlan,
    CoachPlanSubscription,
    CoachLanguage,
    CoachBank
)

from .serializers import (
    CoachSerializer,
    CoachPlanSerializer,
    CoachLanguageSerializer,
    CoachPlanSubscriptionSerializer,
    CoachDetailSerializer,
    CoachBankSerializer
)

# Create your views here.

@api_view(['POST'])
def login(request):
    username = request.data.get('username', None)
    password = request.data.get('password', None)
    if not username or not password:
        raise exceptions.AuthenticationFailed(('No credentials provided.'))

    credentials = {
        get_user_model().USERNAME_FIELD: username,
        'password': password
    }

    user = authenticate(**credentials)
    if user is None:
        raise exceptions.AuthenticationFailed(('Invalid username/password.'))

    if not user.is_active:
        raise exceptions.AuthenticationFailed(('User inactive or deleted.'))

    try:
        client = Coach.objects.get(user=user)
    except:
        raise exceptions.AuthenticationFailed(('Invalid username/password.'))

    refresh = RefreshToken.for_user(user)

    return JsonResponse({
        'refresh': str(refresh),
        'access': str(refresh.access_token),
        'role' :'choach'
 
    })

@api_view(['PUT'])
@permission_classes((IsAuthenticated,))
def changePassword(request):
    old_password = request.data.get('old_password', None)
    password = request.data.get('password', None)
    repeat_password = request.data.get('repeat_password', None)
    
    if repeat_password != password:
       return JsonResponse({
            'message': 'Passwords not match',
        }, status=400)  

    if  not old_password or not password:
        return JsonResponse({
            'message': 'Invalid user input',
        }, status=400)

    auth = get_authorization_header(request).split()
    jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
    payload = jwt_decode_handler(auth[1])
    
    u = User.objects.get(id=payload['user_id'])
    if u.check_password(old_password):
        u.set_password(password)
        u.save()
    else:
        return JsonResponse({
            'message': 'Old password is invalid',
        }, status=400)

    return JsonResponse({
        'message': 'Password successfully changed',
    }, status=200)


@api_view(['GET'])
@permission_classes((IsAuthenticated,IsCoach))
def get_me(request):

    try:
        user = Coach.objects.get(user=request.user)
        client_info = CoachSerializer(
            instance=user, context={'request': request})
    except ObjectDoesNotExist:
        return JsonResponse({
            'message': 'User not found',
        }, status=404)
    return JsonResponse({
            'message': 'ok',
            'data': client_info.data
        }, status=200)


class CoachPagination(pagination.PageNumberPagination):

    page_size = 10000
    page_size_query_param = 'page_size'


class CoachFilter(django_filters.FilterSet):
    class Meta:
        model = Coach
        fields = ["slug"]

class CoachViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    # permission_classes = (IsAuthenticated,IsCoach,)
    queryset = Coach.objects.all()
    serializer_class = CoachSerializer
    detail_serializer_class = CoachDetailSerializer
    pagination_class = CoachPagination
    filterset_class = CoachFilter

    def get_serializer_class(self):
        if self.action == 'retrieve':
            if hasattr(self, 'detail_serializer_class'):
                return self.detail_serializer_class

        return super(CoachViewSet, self).get_serializer_class()

    def perform_destroy(self, instance):
        user = instance.user
        instance.delete()
        user.delete()
    

class CoachPlanViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (IsAuthenticated,IsCoach,)
    queryset = CoachPlan.objects.all()
    serializer_class = CoachPlanSerializer


class CoachPlanSubscriptionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classget_fieldses = (IsAuthenticated,IsCoach,)
    queryset = CoachPlanSubscription.objects.all()
    serializer_class = CoachPlanSubscriptionSerializer

class CoachLanguageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (IsAuthenticated,IsCoach,)
    queryset = CoachLanguage.objects.all()
    serializer_class = CoachLanguageSerializer



class CoachBankViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (IsAuthenticated,IsCoach,)
    queryset = CoachBank.objects.all()
    serializer_class = CoachBankSerializer


class CoachList(generics.ListAPIView):
    queryset = Coach.objects.all()
    serializer_class = CoachSerializer

    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = CoachSerializer(instance=queryset, context=serializer_context, many=True)
        response = []
        for coach in serializer.data:
            coach_marathon = dict(coach)
            temp_marathon = coach_marathon['coach_marathon']
            del coach_marathon['coach_marathon']
            response.append({"modelCoach" :coach_marathon,"modelMarathon":temp_marathon})
        return Response({'response':response})