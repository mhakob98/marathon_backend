from django.contrib import admin
from django.contrib.admin import AdminSite
from django.utils.translation import ungettext_lazy
from .models import (
    Coach,
    CoachBank,
    CoachLanguage,
    CoachPlan,
    CoachPlanSubscription
    
)
from django.contrib.auth.models import Group
from django.contrib.auth.models import User

from django.forms import ModelForm, PasswordInput
from django.contrib import admin


from marathon.admin import myems_admin_site
myems_admin_site.register(Coach)
myems_admin_site.register(CoachBank)
myems_admin_site.register(CoachLanguage)
myems_admin_site.register(CoachPlan)
myems_admin_site.register(CoachPlanSubscription)




