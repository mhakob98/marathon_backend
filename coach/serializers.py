from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.db import transaction
from marathon_main.serializers import MarathonSerializer
from utils.serializers import LanguageSerializer
from career.serializers import (
    CertificatesSerializer,
    EducationSerializer,
    ExperienceSerializer,
    SpecialitySerializer
)
from django.db.models import Q

from career.models import (
    Certificates,
    Education,
    Experience,
    Speciality
)
from .models import (
    Coach,
    CoachPlan,
    CoachPlanSubscription,
    CoachLanguage,
    CoachBank
)
from utils.models import (
    Language
)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(
        style={'input_type': 'password'}, required=False
    )

    email = serializers.EmailField(required=False)

    class Meta:
        model = User
        fields = (
            'id',
            'email',
            'password',
            'first_name',
            'last_name',
        )



class CoachSerializerLocal(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Coach
        fields = (
            'id',
            'url',
            'user',
            'avatar',
            'cover',
            'slug'
        )



class CoachSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer()
    coach_marathon = MarathonSerializer(many=True, read_only=True)
    certificates = CertificatesSerializer(many=True,required=False)
    education = EducationSerializer(many=True,required=False)
    experience = ExperienceSerializer(many=True,required=False)
    languages = LanguageSerializer(many=True, read_only=True)
    is_follower = serializers.SerializerMethodField()
    is_follower_id = serializers.SerializerMethodField()

    class Meta:
        model = Coach
        fields = (
            'id',
            'url',
            'user',
            'status',
            'locationLocality',
            'locationCountry',
            'coach_marathon',
            'google_id',
            'about',
            'rate',
            'ui_language',
            'languages',
            'avatar',
            'certificates',
            'education',
            'experience',
            'language',
            'location',
            'facebook',
            'youtube',
            'instagram',
            'linkedin',
            'speciality',
            'is_follower',
            'cover',
            'is_follower_id',
            'is_faworit',
            'slug'
            
        )

    def create(self, validated_data):
        lans = None
        certificates = None
        educationes = None
        experiences = None
        langs = None
        specialityes = None

        if 'ui_language' in validated_data:
            lans = validated_data['ui_language']
            del validated_data['ui_language']

        if 'speciality' in validated_data:
            specialityes = validated_data['speciality']
            del validated_data['speciality']

        if 'language' in validated_data:
            langs = validated_data['language']
            del validated_data['language']

        if 'certificates' in validated_data:
            certificates = validated_data['certificates']
            del validated_data['certificates']

        if 'education' in validated_data:
            educationes = validated_data['education']
            del validated_data['education']
        
        if 'experience' in validated_data:
            experiences = validated_data['experience']
            del validated_data['experience'] 

        try:
            validated_data['user'] = User.objects.create(
                first_name=validated_data.get('user')['first_name'],
                last_name=validated_data.get('user')['last_name'],
                email=validated_data.get('user')['email'],
                password=make_password(validated_data.get('user')['password']),
                username=validated_data.get('user')['email']
            )
        except IntegrityError as e:
            raise serializers.ValidationError(e)
        
        instance = Coach.objects.create(**validated_data)
       
        if lans:
            instance.ui_language.set(lans)

        if langs:
            instance.language.set(langs)

        if specialityes:
            instance.speciality.set(specialityes)

        if certificates:
            for certificate in certificates:
                ce = Certificates.objects.create(**certificate)
                instance.certificates.add(ce)

        if educationes:
            for educatione in educationes:
                ed = Education.objects.create(**educatione)
                instance.education.add(ed)
        if experiences:
            for experience in experiences:
                ex = Experience.objects.create(**experience)
                instance.experience.add(ex)
        
        instance.save()
        return instance

    @transaction.atomic
    def update(self, instance, validated_data):
        User.objects.filter(id=instance.user.id).update(first_name=validated_data.get('user')['first_name'],
                                                        last_name=validated_data.get('user')['last_name'])
        instance.__dict__.update(**validated_data)
        lans = None
        certificates = None
        educationes = None
        experiences = None
        langs = None
        specialityes = None

        if 'speciality' in validated_data:
            specialityes = validated_data['speciality']
            del validated_data['speciality']

        if 'ui_language' in validated_data:
            lans = validated_data['ui_language']
            del validated_data['ui_language']
        
        if 'language' in validated_data:
            langs = validated_data['language']
            del validated_data['language']

        if 'certificates' in validated_data:
            certificates = validated_data['certificates']
            del validated_data['certificates']

        if 'education' in validated_data:
            educationes = validated_data['education']
            del validated_data['education']
        
        if 'experience' in validated_data:
            experiences = validated_data['experience']
            del validated_data['experience'] 


               
        if lans:
            instance.ui_language.set(lans)

        if langs:
            instance.language.set(langs)

        if specialityes:
            instance.speciality.set(specialityes)
        if certificates:
            instance.certificates.clear()
            for certificate in certificates:
                ce = Certificates.objects.create(**certificate)
                instance.certificates.add(ce)

        if educationes:
            instance.education.clear()
            for educatione in educationes:
                ed = Education.objects.create(**educatione)
                instance.education.add(ed)
        if experiences:
            instance.experience.clear()
            for experience in experiences:
                ex = Experience.objects.create(**experience)
                instance.experience.add(ex)

        instance.save()
        return instance


    def get_is_follower(self, obj):
        request = self.context['request']
        user = request.user
        if obj.whom_coach_follower.filter(Q(Q(who_user__user_id=user.id) & ~Q(who_user__user_id=None)) |Q(Q(who_coach__user_id=user.id) & ~Q(who_coach__user_id=None))).count() > 0  :
            return True
        else :
            return False

    def get_is_follower_id(self, obj):
        request = self.context['request']
        user = request.user
        res = obj.whom_coach_follower.filter(Q(Q(who_user__user_id=user.id) & ~Q(who_user__user_id=None)) |Q(Q(who_coach__user_id=user.id) & ~Q(who_coach__user_id=None)))
        
        if res.count() > 0  :
            print(res,'-----------')
            return res[0].id

        else :
            return False

class CoachDetailSerializer(serializers.HyperlinkedModelSerializer):
    coach_user = UserSerializer(source='user', read_only=True)
    is_follower = serializers.SerializerMethodField()
    is_follower_id = serializers.SerializerMethodField()
    class Meta:
        model = Coach
        fields = (
            'id',
            'coach_user',
            'avatar',
            'url',
            'status',
            'about',
            'rate',
            'locationLocality',
            'locationCountry',
            'ui_language',
            'is_follower',
            'cover',
            'is_follower_id',
            'is_faworit',
            'slug'
        )

    def get_is_follower(self, obj):
        request = self.context['request']
        user = request.user
        if obj.whom_coach_follower.filter(Q(Q(who_user__user_id=user.id) & ~Q(who_user__user_id=None)) |Q(Q(who_coach__user_id=user.id) & ~Q(who_coach__user_id=None))).count() > 0  :
            return True
        else :
            return False

    def get_is_follower_id(self, obj):
        request = self.context['request']
        user = request.user
        res = obj.whom_coach_follower.filter(Q(Q(who_user__user_id=user.id) & ~Q(who_user__user_id=None)) |Q(Q(who_coach__user_id=user.id) & ~Q(who_coach__user_id=None)))
        
        if res.count() > 0  :
            print(res,'-----------')
            return res[0].id

        else :
            return False
            
class CoachPlanSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CoachPlan
        fields = '__all__'


class CoachPlanSubscriptionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CoachPlanSubscription
        fields = '__all__'


class CoachLanguageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CoachLanguage
        fields = '__all__'

class CoachBankSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CoachBank
        fields = '__all__'
