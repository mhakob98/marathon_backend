
from django.urls import path,include
from coach import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'coach', views.CoachViewSet)
router.register(r'plan', views.CoachPlanViewSet)
router.register(r'plan/subscription', views.CoachPlanSubscriptionViewSet)
router.register(r'language', views.CoachLanguageViewSet)
router.register(r'bank', views.CoachBankViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('get/me', views.get_me),
    path('coach',views.CoachViewSet),
    path('coach-list',views.CoachList.as_view())
]

