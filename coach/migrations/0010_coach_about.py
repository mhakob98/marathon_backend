# Generated by Django 2.0.13 on 2020-03-03 12:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coach', '0009_auto_20200217_0806'),
    ]

    operations = [
        migrations.AddField(
            model_name='coach',
            name='about',
            field=models.TextField(null=True),
        ),
    ]
